var i = 10;
var idTextTime = 'time_to_update';
var idLocationHref = 'href_to_update';

function startNextUpdateWithParams(pIdLocationHref, pIdTextTime, countSeconds) {
    i = countSeconds;
    idTextTime = pIdTextTime;
    idLocationHref = pIdLocationHref;
//    alert(i + '-' + idLocationHref + ' - ' + idTextTime);
    startNextUpdate();
}

function startNextUpdate() {
//    alert(i + '-' + idLocationHref + ' - ' + idTextTime);
    
    
    i--;
    if(i <= 0){
        document.getElementById(idTextTime).innerHTML = ' - Старт';
        location.href = document.getElementById(idLocationHref).href;
        return false;
    }
    window.setTimeout("startNextUpdate()", 1000);
    document.getElementById(idTextTime).innerHTML = ' - ' + i;

    return false;
}

function showHideElement(id) {
    if (document.getElementById(id).style.display == 'block') {
        document.getElementById(id).style.display = 'none'
    } else {
        document.getElementById(id).style.display = 'block'
    }
}