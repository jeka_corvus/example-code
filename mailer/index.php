<html>
    <head>
        <title>Mailer Index</title>
        <style>
           table { 
            border: 4px double black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
           }
           th { 
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           td { 
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           .dark {
                background: #ccc; /* Цвет фона ячеек */
           }
           textarea {
               width: 800px;
               height: 150px;
           }
           input[type=text] {
                width: 800px;
           } 
        </style>          
    </head>
    <body>

        <h2><a href="./resource.php">Ресурсы</a></h2>
        </br>
        </br>
        </br>
        <h2><a href="./template.email.php">Шаблоны электронных писем</a></h2>

    </body>    
</html>
