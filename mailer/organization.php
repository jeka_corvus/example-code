<?php
require_once('default.conf.php');
require_once('db.controller.php');
require_once('utility/paginator.php');

if (isset($_GET['resource_id'])) {
    $resourceGeneral = new MailerResource($_GET['resource_id']);
} else {
    echo "В адресной строке нет ID ресурса!!!";
    die;
}

$pageParams = "";
if (isset($_GET['page'])) {
    $pageParams = '&page=' . $_GET['page'];
}

if (isset($_POST['id'])) {
    if ($_POST['name'] != "" && $_POST['uri'] != "") {
        $mailerOrganization = new MailerOrganization($_POST['id']);
        $mailerOrganization->name       = $_POST['name'];
        $mailerOrganization->uri        = $_POST['uri'];
        $mailerOrganization->done       = isset($_POST['done']) ? 1 : 0;
                
        if ($_POST['id'] == "") {
            $mailerOrganization->resource_id = $resourceGeneral->id;
            $mailerOrganization->create();
        } else {
            $mailerOrganization->update();
        }
        
        header("Location: ./organization.php?resource_id=$resourceGeneral->id" . $pageParams);
    }
}


if (isset($_GET['action'])) {
    if (isset($_GET['id'])) {
        $actionOrganization = new MailerOrganization($_GET['id']);
    }
    
    switch ($_GET['action']){
        case "edit":
            break;
        
        case "delete":
            $actionOrganization->delete();
            header("Location: ./organization.php?resource_id=$resourceGeneral->id" . $pageParams);
            break;
        
        case "clear_organizations":
            MailerOrganization::clearOrganizations($resourceGeneral->id);
            header("Location: ./organization.php?resource_id=$resourceGeneral->id" . $pageParams);
            break;
        
        case "update_next_organization":
            if (!empty($resourceGeneral) && !empty($resourceGeneral->script) && ($resourceGeneral->uri_next_organization != "")) {
                require_once('resource/' . $resourceGeneral->script);
                $resourceObject = new ResourceClass();
                $resourceObject->setMailerResource($resourceGeneral);
                $resourceObject->updateOrganizations();
                header("Location: ./organization.php?resource_id=$resourceGeneral->id&auto_action=update_next_organization" . $pageParams);
            } else {
                header("Location: ./organization.php?resource_id=$resourceGeneral->id" . $pageParams);
            }
            break;

        default:
            header("Location: ./organization.php?resource_id=$resourceGeneral->id" . $pageParams);
            break;
    }
}

?>
<html>
    <head>
        <title>Mailer Organization</title>
        <script type="text/javascript" src="./js/utility.js"></script>
        <?php if (isset($_GET['auto_action']) && $_GET['auto_action'] == 'update_next_organization') { ?>
            <script type="text/javascript">startNextUpdate();</script>
        <?php } ?>
        <style>
           table { 
            border: 4px double black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
           }
           th { 
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           td { 
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           .dark {
                background: #ccc; /* Цвет фона ячеек */
           }
           textarea {
               width: 800px;
               height: 150px;
           }
           input[type=text] {
                width: 800px;
           }
           .active_page {
               font-size: 25px;
           }
        </style>          
    </head>
    <body>

    <h2><a href="#" onclick="showHideElement('block_add_edit'); return false;">Добавить/Редактировать организацию</a></h2>
    <div id="block_add_edit" style="display: <?php echo isset($actionOrganization) ? "block" : "none"; ?>;">
        <h2>Добавить\Редактировать организацию</h2>
        <form action="<?php echo "./organization.php?resource_id=$resourceGeneral->id" . $pageParams ?>" name="add_edit_organization" method="post">
            <table>
                <tr>
                    <th><b>ID</b></th>
                    <td>
                        <?php echo isset($actionOrganization) ? $actionOrganization->id : ""; ?>
                        <input type="hidden" name="id" value="<?php echo isset($actionOrganization) ? $actionOrganization->id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Ресурс ID</b></th>
                    <td>
                        <?php echo isset($actionOrganization) ? $actionOrganization->resource_id : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Название</b></th>
                    <td>
                        <input type="text" name="name" value="<?php echo isset($actionOrganization) ? $actionOrganization->name : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Ссылка</b></th>
                    <td>
                        <input type="text" name="uri" value="<?php echo isset($actionOrganization) ? $actionOrganization->uri : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Закончен</b></th>
                    <td>
                        <input type="checkbox" name="done" <?php echo (isset($actionOrganization) && $actionOrganization->done) ? "checked" : ""; ?> />
                    </td>
                </tr>
                <tr>
                    <th><b>Дата создания</b></th>
                    <td>
                        <?php echo isset($actionOrganization) ? $actionOrganization->created_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Дата обновления</b></th>
                    <td>
                        <?php echo isset($actionOrganization) ? $actionOrganization->updated_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <button type="submit">Сохранить</button>
                        </form>
                        &nbsp;&nbsp;&nbsp;<a href="<?php echo "./organization.php?resource_id=$resourceGeneral->id" . $pageParams ?>">Отмена</a>
                    </td>
                </tr>
            </table>        
    </div>
            <a id='href_to_update' href="<?php echo "./organization.php?resource_id=$resourceGeneral->id&action=update_next_organization" . $pageParams ?>">
                <h1>Обновить организации<b id='time_to_update'></b></h1>
            </a>
            <a href='<?php echo "./organization.php?resource_id=$resourceGeneral->id" . $pageParams ?>'>Отмена авто обновления</a>
            
        <h2>Статистика</h2>
        <table>
            <tr>
                <th><b>Всех организаций</b></th>
                <th><b>Готовых организаций</b></th>
                <th><b>Незаконченых организаций</b></th>
            </tr>
            <tr>
                <td><b><?php echo $countOrganizationsAll = MailerOrganization::getCountByResource($resourceGeneral->id) ?></b></td>
                <td style='color: green'><b><?php echo $countOrganizationsIsDone = MailerOrganization::getCountByResourceIsDone($resourceGeneral->id) ?></b></td>
                <td style='color: red'><b><?php echo $countOrganizationsAll - $countOrganizationsIsDone ?></b></td>
            </tr>
        </table>
        <br/>
        <h1><a href="<?php echo "./organization.php?resource_id=$resourceGeneral->id&action=clear_organizations" . $pageParams ?>" onclick="return confirm('Вы уверены, что хотите почистить организации?')">Почистить организации</a></h1>
        <br/>
        <?php 
            $backLink = "-><a href='./resource.php'>$resourceGeneral->name</a>"
                    . "-><a href='./contact.php?resource_id=$resourceGeneral->id'>Контакты</a>";
            
            $paginator = new Paginator();
            $paginator->countAllItems = $countOrganizationsAll;
            $paginator->uri = "./organization.php?resource_id=$resourceGeneral->id";
            $paginator->activePage = isset($_GET['page']) ? $_GET['page'] : 1;
            $contentPagination = $paginator->render();
        ?>
        <h2><a href="./">Home</a><?php echo $backLink ?></h2>
        <h2>Список организаций</h2>
        <br />
        <?php echo $contentPagination; ?>
        <br />
        <table>
            <tr>
                <th><b>#</b></th>
                <th><b>ID</b></th>
                <th><b>Ресурс</b></th>
                <th><b>Название</b></th>
                <th><b>Ссылка</b></th>
                <th><b>Закончен</b></th>
                <th><b>Дата создания</b></th>
                <th><b>Дата обновления</b></th>
                <th><b>Действия</b></th>
            </tr>
<?php             
        $organizations = MailerOrganization::getOrganizationsByResource($resourceGeneral->id,
                ($paginator->activePage - 1) * $paginator->countItemsToPage, $paginator->countItemsToPage);
        
        $index = ($paginator->activePage - 1) * $paginator->countItemsToPage;
        foreach ($organizations as $key => $organization) {
            $outText = "<tr " . (($index % 2) ? "class='dark'" : "") . ">"
                . "<td>" . ($index++ + 1) . "</td>"
                . "<td>$organization->id</td>"
                . "<td>$organization->resource_id</td>"
                . "<td>$organization->name</td>"
                . "<td><a href='$resourceGeneral->uri$organization->uri' target='_blank'>Ссылка на организацию</a></td>"
                . "<td><b style='color:" . ($organization->done ? "green'>Да" : "red'>Нет") . "</b></td>"
                . "<td>$organization->created_at</td>"
                . "<td>$organization->updated_at</td>"
                . "<td><table style='border: 0px;'>"
                    . "<tr><td><a href='./organization.php?resource_id=$resourceGeneral->id&id=$organization->id&action=delete$pageParams' onclick=\"return confirm('Вы уверены, что хотите удалить - \'$organization->name\'?')\">Удалить</a><br /><br /></td></tr>"
                    . "<tr><td><a href='./organization.php?resource_id=$resourceGeneral->id&id=$organization->id&action=edit$pageParams'>Редактировать</a><br /><br /></td></tr>"
                . "</table></td>"
            . "</tr>";
            echo $outText;
        }
?>
        </table>
        <br />
        <?php echo $contentPagination; ?>
    </body>    
</html>