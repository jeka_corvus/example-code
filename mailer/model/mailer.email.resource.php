<?php
require_once('model.php');

class MailerEmailResource extends Model {
    private static $tableName = "mailer_email_resource";
    
    public $id = 0;
    public $email_id;
    public $resource_id;
    public $created_at;
    
    public function __construct($id = 0) {
        parent::__construct();
        
        $this->listRows = array(
        'id', 
        'email_id', 
        'resource_id',
        'created_at');
        
        if ($id != 0 && $id != "" && !empty($id)) {
            $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `id`=" . $id;
            $result = DBController::getInstance()->query($sql);
            if (count($result->row) > 0) {
                $this->mappingFromArray($result->row);
            }
        }
    }
    
    public function create() {
        $sql = "INSERT INTO `" . self::$tableName . "` SET ";

        $dateNow = $this->getDateNow();
        
        $this->created_at = $dateNow;
        
        $sql = $this->mappingSQL($sql);
        DBController::getInstance()->query($sql);

        $this->id = DBController::getInstance()->getLastId();
    }

    public function update() {
        $sql = "UPDATE `" . self::$tableName . "` SET ";

        $sql = $this->mappingSQL($sql);

        $sql .= " WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public function delete() {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }

    public function isExist() {
        $sql = "SELECT * FROM `" . self::$tableName . "` WHERE"
                . " `resource_id`=$this->resource_id"
                . " AND `email_id`='$this->email_id'";
        
        $result = DBController::getInstance()->query($sql);

        if (count($result->row) > 0) {
            $this->mappingFromArray($result->row);
        }
        
        return $this->id > 0 ? true : false;
    }
    
    public static function deleteByEmailId($emailId) {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `email_id`=" . $emailId;
        DBController::getInstance()->query($sql);
    }
}