<?php
include('utility/mail_mime/Mail.php'); 
include('utility/mail_mime/Mail/mime.php');

class Mailer {
    
    private $mailerEmail;
    private $headers;
    private $body;

    public function __construct(MailerEmail $mailerEmail) {
        $this->mailerEmail = $mailerEmail;
    }
    
    public function sendEmails($listContacts) {
        $this->initEmail();
        $mail =& Mail::factory('mail');
        
        foreach ($listContacts as $contact) {
            $listEmails = explode("|", $contact->email);
            unset($listEmails[count($listEmails) - 1]);
            unset($listEmails[0]);
            $mailerEmailContact = new MailerEmailContact();
            $mailerEmailContact->contact_id = $contact->id;
            $mailerEmailContact->email_id = $this->mailerEmail->id;
            $mailerEmailContact->getByEmailAndContact();
            $listEmailsIsSend = explode(";", $mailerEmailContact->list_emails);
            
            $listEmailsToSend = array_diff($listEmails, $listEmailsIsSend);
            
            foreach ($listEmailsToSend as $emailTo) {
                $isSend = $mail->send($emailTo, $this->headers, $this->body);
                if ($isSend === true) {
                    $mailerEmailContact->list_emails .= 
                            (empty($mailerEmailContact->list_emails) ? "" : ";") . $emailTo;
                } else {
                    $mailerEmailContact->createOrUpdate();
                    return false;
                }
            }
            $mailerEmailContact->done = 1;
            $mailerEmailContact->createOrUpdate();
            $contact->updateDateLastSend();
        }
        
        return true;
    }
    
    public function initEmail() {
        $params = array(
            'eol' => "\r\n",
            'html_charset'  => 'UTF-8',
            'text_charset'  => 'UTF-8',
            'head_charset'  => 'UTF-8'
        );
        $mime = new Mail_mime($params);

        $listImages = explode(';', $this->mailerEmail->image);
        foreach ($listImages as $image) {
            $file = ConfigMailer::$rootDir . ConfigMailer::$dirImagesEmail . $image;
            $mime->addHTMLImage($file, 'image/jpeg', $image);
        }
        $mime->setHTMLBody($this->mailerEmail->body_email);
        $this->body = $mime->get();

        // обязательно $mime->get() должно быть перед генерацией $mime->headers()
        $headersList = array(
                      'From'    => $this->mailerEmail->from,
                      'Subject' => $this->mailerEmail->subject,
                      );
        $this->headers = $mime->headers($headersList);
    }
}
