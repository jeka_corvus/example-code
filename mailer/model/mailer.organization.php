<?php
require_once('model.php');

class MailerOrganization extends Model {
    private static $tableName = "mailer_organization";
    
    public $id = 0;
    public $resource_id;
    public $name;
    public $uri;
    public $done = 0;
    public $created_at;
    public $updated_at;
    
    public function __construct($id = 0) {
        parent::__construct();
        
        $this->listRows = array(
        'id',
        'resource_id',
        'name', 
        'uri',
        'done',
        'created_at',
        'updated_at');
        
        if ($id != 0 && $id != "" && !empty($id)) {
            $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `id`=" . $id;
            $result = DBController::getInstance()->query($sql);
            if (count($result->row) > 0) {
                $this->mappingFromArray($result->row);
            }
        }
    }
    
    public function create() {
        $sql = "INSERT INTO `" . self::$tableName . "` SET ";

        $dateNow = $this->getDateNow();
        
        $this->created_at = $dateNow;
        $this->updated_at = $dateNow;
        
        $sql = $this->mappingSQL($sql);
        DBController::getInstance()->query($sql);

        $this->id = DBController::getInstance()->getLastId();
    }

    public function update() {
        $sql = "UPDATE `" . self::$tableName . "` SET ";

        $this->updated_at = $this->getDateNow();
        $sql = $this->mappingSQL($sql);

        $sql .= " WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public function delete() {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public static function clearOrganizations($resourceId) {
        MailerContact::deleteContactWithEmptyEmails($resourceId);

        $sql = "DROP TABLE IF EXISTS `t_temp`";
        DBController::getInstance()->query($sql);
        $sql = "CREATE TEMPORARY TABLE `t_temp` AS (SELECT id FROM " . self::$tableName . " WHERE 
        id NOT IN (SELECT mo.id FROM " . self::$tableName . " AS mo RIGHT JOIN mailer_contact AS mc 
        ON mo.id=mc.organization_id WHERE mo.resource_id=$resourceId) AND resource_id=$resourceId)";
        DBController::getInstance()->query($sql);
        $sql = "UPDATE " . self::$tableName . " SET done=0 WHERE id IN (SELECT id FROM t_temp)";
        DBController::getInstance()->query($sql);
        $sql = "DROP TABLE IF EXISTS `t_temp`";
        DBController::getInstance()->query($sql);
        $sql = "DELETE FROM " . self::$tableName . " WHERE done=0 AND resource_id=$resourceId";
        DBController::getInstance()->query($sql);
    }

    public static function getCountByResource($resourceId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` WHERE `resource_id`='$resourceId'";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public static function getCountByResourceIsDone($resourceId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName 
                . "` WHERE `resource_id`='$resourceId' AND `done`=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public function isExist() {
        $sql = "SELECT * FROM `" . self::$tableName . "` WHERE"
                . " `resource_id`=$this->resource_id"
                . " AND `name`='$this->name' AND `uri`='$this->uri'";
        
        $result = DBController::getInstance()->query($sql);

        if (count($result->row) > 0) {
            $this->mappingFromArray($result->row);
        }
        
        return $this->id > 0 ? true : false;
    }
    
    public static function getOrganizationsByResource($resourceId, $limitStart = 0, $limitCount = 0) {
        $sql = "SELECT * FROM `" . self::$tableName . "`"
               . " WHERE `resource_id`='$resourceId' ORDER BY `id` ASC";
        if ($limitCount != 0) {
            $sql .= " LIMIT $limitStart, $limitCount";
        }
        $result = DBController::getInstance()->query($sql);
        
        $listOrganizations = array();
        foreach ($result->rows as $organization) {
            $mailerOrganization = new MailerOrganization();
            $mailerOrganization->mappingFromArray($organization);
            $listOrganizations[] = $mailerOrganization;
        }
        
        return $listOrganizations;
    }

    public function getNextOrganizationNotDone($resourceId) {
        $sql = "SELECT * FROM `" . self::$tableName . "`"
                . " WHERE `resource_id`='$resourceId' AND `done`=0 ORDER BY `id` LIMIT 1";

        $result = DBController::getInstance()->query($sql);
        if (count($result->row) > 0) {
            $this->mappingFromArray($result->row);
        }
    }
    
    public static function deleteByResource($resourceId) {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `resource_id`=" . $resourceId;
        DBController::getInstance()->query($sql);
    }
}