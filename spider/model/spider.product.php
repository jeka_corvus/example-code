<?php
require_once('model.php');

class SpiderProduct extends Model {
    private static $tableName = "spider_product";
    
    public $id = 0;
    public $category_id;
    public $name;
    public $model = "";
    public $site_product_id = 0;
    public $link;
    public $image = "";
    public $description = "";
    public $price = "";
    public $meta_description = "";
    public $done = 0;
    public $created_at;
    public $updated_at;
    
    public function __construct($id = 0) {
        parent::__construct();
        
        $this->listRows = array(
        'id',
        'category_id',
        'site_product_id',
        'name', 
        'model', 
        'link',
        'image',
        'description',
        'price',
        'meta_description',
        'done',
        'created_at',
        'updated_at');
        
        if ($id != 0 && $id != "" && !empty($id)) {
            $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `id`=" . $id;
            $spiderProduct = DBController::getInstance()->query($sql);
            if (count($spiderProduct->row) > 0) {
                $this->mappingFromArray($spiderProduct->row);
            }
        }
    }
    
    public static function getProductsByCategory($categoryId) {
        $sql = "SELECT * FROM `" . self::$tableName . "`"
               . " WHERE `category_id`='$categoryId' ORDER BY `updated_at` DESC, `id` ASC";
        $result = DBController::getInstance()->query($sql);
        
        $listProducts = array();
        foreach ($result->rows as $product) {
            $spiderProduct = new SpiderProduct();
            $spiderProduct->mappingFromArray($product);
            $listProducts[] = $spiderProduct;
        }
        
        return $listProducts;
    }
    
    public function getNextProductsNotDone($categoryId) {
        $sql = "SELECT * FROM `" . self::$tableName . "`"
               . " WHERE `category_id`='$categoryId' AND `done`=0 ORDER BY `id` LIMIT 1";

        $spiderProduct = DBController::getInstance()->query($sql);
        if (count($spiderProduct->row) > 0) {
            $this->mappingFromArray($spiderProduct->row);
        }
    }

    public static function getCountByCategory($categoryId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` WHERE `category_id`='$categoryId'";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public static function getCountByCategoryIsDone($categoryId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` WHERE `category_id`='$categoryId'"
                . " AND `done`=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public function create() {
        $sql = "INSERT INTO `" . self::$tableName . "` SET ";

        $this->meta_description = ConfigSpider::$metaDescriptionProduct;

        $dateNow = $this->getDateNow();
        
        $this->created_at = $dateNow;
        $this->updated_at = $dateNow;
        
        $sql = $this->mappingSQL($sql);
        
        DBController::getInstance()->query($sql);

        $this->id = DBController::getInstance()->getLastId();
    }

    public function isExist() {
        $sql = "SELECT * FROM `" . self::$tableName . "` WHERE"
                . " category_id='$this->category_id' AND link='$this->link'";
        
        $result = DBController::getInstance()->query($sql);

        if (count($result->row) > 0) {
            $this->mappingFromArray($result->row);
        }
        
        return $this->id > 0 ? true : false;
    }

    public function update() {
        $sql = "UPDATE `" . self::$tableName . "` SET ";

        $this->updated_at = $this->getDateNow();
        $sql = $this->mappingSQL($sql);

        $sql .= " WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public function delete() {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `id`=$this->id";
        DBController::getInstance()->query($sql);
        $this->deleteImage();
    }
    
    public function deleteImage() {
        if (empty($this->image)) {
            return;
        }
        
        if (file_exists(ConfigSpider::getPathToImages() . $this->image)) {
            @unlink(ConfigSpider::getPathToImages() . $this->image);
        }
    }
    
    public function deleteByListCategories($listIdCategories) {
        $sql = "SELECT `image` FROM `" . self::$tableName . "` WHERE `category_id` IN (" . join(',', $listIdCategories) . ")";
        $listImages = DBController::getInstance()->query($sql);
        
        foreach ($listImages->rows as $productImage) {
           $this->image = $productImage['image'];
           $this->deleteImage();
        }
        
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `category_id` IN (" . join(',', $listIdCategories) . ")";
        DBController::getInstance()->query($sql);
    }
    
    public function setEmptySiteId($listIdCategories) {
        $sql = "UPDATE `" . self::$tableName . "` SET `site_product_id`=0"
                . " WHERE `category_id` IN (" . join(',', $listIdCategories) . ")";
        DBController::getInstance()->query($sql);
    }
    
    public static function synchronizationWithSiteByCategory($categoryId) {
        $listProducts = self::getProductsByCategory($categoryId);
        foreach ($listProducts as $product) {
            $product->synchronizationWithSite();
        }
    }

    public function synchronizationWithSite() {
        if ($this->done != 1) {
            return;
        }
        
        if ($this->site_product_id == 0) {
            $this->createInSite();
        } else {
            $this->updateInSite();
        }
    }
    
    public function createInSite() {
        $dateNow = $this->getDateNow();
        $category = new SpiderCategory($this->category_id);
        $manufacturer = new SpiderManufacturer($category->manufacturer_id);
        $image = $this->moveImage($manufacturer);

        $slq = "INSERT INTO `oc_product` SET"
                . " `model`='$this->model'"
                . ", `sku`=''"
                . ", `upc`=''"
                . ", `ean`=''"
                . ", `jan`=''"
                . ", `isbn`=''"
                . ", `mpn`=''"
                . ", `location`=''"
                . ", `quantity`=1"
                . ", `stock_status_id`=5"
                . ", `image`='$image'"
                . ", `manufacturer_id`=$manufacturer->site_manufacturer_id"
                . ", `price`='$this->price'"
                . ", `tax_class_id`=0"
                . ", `date_available`='$dateNow'"
                . ", `weight_class_id`=1"
                . ", `length_class_id`=1"
                . ", `sort_order`=1"
                . ", `status`=0"
                . ", `date_added`='$dateNow'"
                . ", `date_modified`='$dateNow'";
        DBController::getInstance()->query($slq);
        $this->site_product_id = DBController::getInstance()->getLastId();
        $this->update();

        $slq = "INSERT INTO `oc_product_description` SET"
                . " `product_id`=$this->site_product_id"
                . ", `language_id`=1"
                . ", `name`='$this->name'"
                . ", `description`='$this->description'"
                . ", `description_mini`='$this->description'"
                . ", `meta_description`='$this->meta_description'"
                . ", `meta_keyword`=''"
                . ", `seo_title`='$this->name'"
                . ", `seo_h1`='$this->name'"
                . ", `tag`=''";
        DBController::getInstance()->query($slq);

        $slq = "INSERT INTO `oc_product_reward` SET"
                . " `product_id`=$this->site_product_id"
                . ", `customer_group_id`=1"
                . ", `points`=0";
        DBController::getInstance()->query($slq);

        $slq = "INSERT INTO `oc_product_to_category` SET"
                . " `product_id`=$this->site_product_id"
                . ", `category_id`=$category->site_category_id"
                . ", `main_category`=1";
        DBController::getInstance()->query($slq);
        
        $slq = "INSERT INTO `oc_product_to_store` SET"
                . " `product_id`=$this->site_product_id"
                . ", `store_id`=0";
        DBController::getInstance()->query($slq);
        
        $slq = "INSERT INTO `oc_url_alias` SET"
                . " `query`='product_id=$this->site_product_id'"
                . ", `keyword`='" . Utility::encodeLowerStringToTranslitTrim($this->name) . "'"
                . ", `seomanager`=0";
        DBController::getInstance()->query($slq);
    }
    
    private function moveImage(SpiderManufacturer $manufacturer) {
        $pathToImage = ConfigSpider::$folderToImagesSite2 
                        . Utility::encodeLowerStringToTranslitTrim($manufacturer->name);
        $fullPath = ConfigSpider::$rootDir . "/../../" . ConfigSpider::$folderToImagesSite1;
        
        if (!file_exists($fullPath . $pathToImage)) {
            mkdir($fullPath . $pathToImage, 0755);
        }
        
        $pathToImage .=  "/" . $this->image;
        
        copy(ConfigSpider::getPathToImages() . $this->image, $fullPath . $pathToImage);
        
        return $pathToImage;
    }
    
    public function updateInSite() {
        $dateNow = $this->getDateNow();
        $category = new SpiderCategory($this->category_id);
        $manufacturer = new SpiderManufacturer($category->manufacturer_id);
        $image = $this->moveImage($manufacturer);

        $slq = "UPDATE `oc_product` SET"
                . " `model`='$this->model'"
                . ", `image`='$image'"
                . ", `price`='$this->price'"
                . ", `date_modified`='$dateNow'"
                . " WHERE `product_id`=$this->site_product_id";
        DBController::getInstance()->query($slq);
        
        $slq = "UPDATE `oc_product_description` SET"
                . " `name`='$this->name'"
                . ", `description`='$this->description'"
                . ", `description_mini`='$this->description'"
                . ", `meta_description`='$this->meta_description'"
                . ", `seo_title`='$this->name'"
                . ", `seo_h1`='$this->name'"
                . " WHERE `product_id`=$this->site_product_id";
        DBController::getInstance()->query($slq);
        
        $slq = "UPDATE `oc_url_alias` SET"
                . " `keyword`='" . Utility::encodeLowerStringToTranslitTrim($this->name) . "'"
                . " WHERE `query`='product_id=$this->site_product_id'";
        DBController::getInstance()->query($slq);
    }
    
}