var i = 10;
var idTextTime = 'time_to_update';
var idLocationHref = 'href_to_update';

function startUpdateProducts() {
    i--;
    if(i <= 0){
        document.getElementById(idTextTime).innerHTML = ' - Старт';
        location.href = document.getElementById(idLocationHref).href;
        return false;
    }
    window.setTimeout("startUpdateProducts()", 1000);
    document.getElementById(idTextTime).innerHTML = ' - ' + i;

    return false;
}