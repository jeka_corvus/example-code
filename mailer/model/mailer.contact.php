<?php
require_once('model.php');

class MailerContact extends Model {
    private static $tableName = "mailer_contact";
    
    public $id = 0;
    public $organization_id = 0;
    public $name;
    public $title;
    public $phone;
    public $site;
    public $email;
    public $emails_done = 0;
    public $done = 0;
    public $created_at;
    public $updated_at;
    public $date_last_send;
    
    public function __construct($id = 0) {
        parent::__construct();
        
        $this->listRows = array(
        'id',
        'organization_id',
        'name', 
        'title',
        'phone',
        'site',
        'email',
        'emails_done',
        'done',
        'created_at',
        'updated_at',
        'date_last_send');
        
        if ($id != 0 && $id != "" && !empty($id)) {
            $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `id`=" . $id;
            $result = DBController::getInstance()->query($sql);
            if (count($result->row) > 0) {
                $this->mappingFromArray($result->row);
            }
        }
    }
    
    public function create() {
        $sql = "INSERT INTO `" . self::$tableName . "` SET ";

        $dateNow = $this->getDateNow();
        
        $this->created_at = $dateNow;
        $this->updated_at = $dateNow;
        
        $sql = $this->mappingSQL($sql);
        DBController::getInstance()->query($sql);

        $this->id = DBController::getInstance()->getLastId();
    }

    public function update() {
        $sql = "UPDATE `" . self::$tableName . "` SET ";

        $this->updated_at = $this->getDateNow();
        $sql = $this->mappingSQL($sql);

        $sql .= " WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public function delete() {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }

    public function updateDateLastSend() {
        $this->date_last_send = $this->getDateNow();
        $this->update();
    }
    
    public static function getCountByResource($resourceId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` as mc"
                . " LEFT JOIN `mailer_organization` as mo ON mc.`organization_id`=mo.`id`"
                . " WHERE mo.`resource_id`='$resourceId'";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public static function getCountByResourceIsDone($resourceId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` as mc"
                . " LEFT JOIN `mailer_organization` as mo ON mc.`organization_id`=mo.`id`"
                . " WHERE mo.`resource_id`='$resourceId' AND mc.`done`=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public static function getCountByResourceIsDoneEmails($resourceId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` as mc"
                . " LEFT JOIN `mailer_organization` as mo ON mc.`organization_id`=mo.`id`"
                . " WHERE mo.`resource_id`='$resourceId' AND mc.`emails_done`=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public function isExist($resourceId) {
        $sql = "SELECT mc.* FROM `" . self::$tableName . "` as mc"
                . " LEFT JOIN `mailer_organization` as mo ON mc.`organization_id`=mo.`id`"
                . " WHERE mo.`resource_id`='$resourceId'"
                . " AND `title`='$this->title' AND `site`='$this->site'";
        
        $result = DBController::getInstance()->query($sql);

        if (count($result->row) > 0) {
            $this->mappingFromArray($result->row);
        }
        
        return $this->id > 0 ? true : false;
    }
    
    public function isExistEmail($email) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "`"
                . " WHERE `email` LIKE '%|$email|%' AND `id`<>$this->id";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'] > 0 ? true : false;
    }

    public static function getContactsByResource($resourceId, $limitStart = 0, $limitCount = 0) {
        $sql = "SELECT mc.* FROM `" . self::$tableName . "` as mc"
                . " LEFT JOIN `mailer_organization` as mo ON mc.`organization_id`=mo.`id`"
                . " WHERE mo.`resource_id`='$resourceId' ORDER BY mc.`id` ASC";
        if ($limitCount != 0) {
            $sql .= " LIMIT $limitStart, $limitCount";
        }
        $result = DBController::getInstance()->query($sql);
        
        $listContacts = array();
        foreach ($result->rows as $contact) {
            $mailerContact = new MailerContact();
            $mailerContact->mappingFromArray($contact);
            $listContacts[] = $mailerContact;
        }
        
        return $listContacts;
    }

    public static function getNextContactNotEmailsDone($resourceId) {
        $sql = "SELECT mc.* FROM `" . self::$tableName . "` as mc"
                . " LEFT JOIN `mailer_organization` as mo ON mc.`organization_id`=mo.`id`"
                . " WHERE mo.`resource_id`='$resourceId' AND mc.`emails_done`=0";
        $result = DBController::getInstance()->query($sql);
        $mailerContact = null;
        if (count($result->row) > 0) {
            $mailerContact = new MailerContact();
            $mailerContact->mappingFromArray($result->row);
        }
        return $mailerContact;
    }
 
    public static function clearAllContactEmails($resourceId) {
        $limitStart = 0;
        do {
            $listContacts = MailerContact::getContactsByResource($resourceId, $limitStart, 20);
            $limitStart += 20;
            
            foreach ($listContacts as $contact) {
                $emails = $contact->email;
                $contact->clearEmails();
                if ($emails != $contact->email) {
                    $contact->update();
                }
            }
        } while(count($listContacts) > 0);
    } 
    
    public function clearEmails() {
        $newListEmails = array();
        $listEmails = explode(";", $this->email);
        foreach ($listEmails as $email) {
            // Нужно добить регулярное выражение
//                    if (preg_match('/\b([a-z0-9._-]+@[a-z0-9.-]+\.(^(?:jp(?:e?g|e|2)|gif|png|tiff?|bmp|ico)]$)[a-z0-9.-]+)\b/i', $email)) {
            // Сначала проверка но то, чтобы была точка в части после @
            // Затем проверка на не нужные окончание email
            if (preg_match('/\b([a-z0-9._-]+@[a-z0-9.-]+\.[a-z0-9.-]+)\b/i', $email)
                    && !preg_match('/\b(\.jp(?:e?g|e|2)|gif|png|tiff?|bmp|ico$)\b/i', $email)) {
                $newListEmails[] = $email;
            }
        }

//        echo "<pre>";
//        print_r($this);
//        echo "<br />";
//        print_r($listEmails);
//        echo "<br />";
//        print_r($newListEmails);
//        echo "</pre>" . PHP_EOL;
//        die;

        $this->email = join(";", $newListEmails);
//        $this->update();
    }
    
    public function clearEmailsOld() {
        $listExclusion = array('png', 'jpg', 'jpeg', 'tiff', 'tif', 'gif', 'bmp', 'ico');
            
        $newListEmails = array();
        $listEmails = explode(";", $this->email);
        foreach ($listEmails as $email) {
            if (preg_match('/\b([a-z0-9._-]+@[a-z0-9.-]+\.[a-z0-9.-]+)\b/i', $email)) {
                $extention = substr(strrchr($email, '.'), 1);
                if (!in_array(strtolower($extention), $listExclusion)) {
                    $newListEmails[] = $email;
                }
            }
        }

        $this->email = join(";", $newListEmails);
    }
 
    public static function deleteContactWithEmptyEmails($resourceId) {
        MailerEmailContact::deleteWithEmptyEmails($resourceId);
        $sql = "DELETE FROM `" . self::$tableName . "`"
                . " USING `" . self::$tableName . "`, `mailer_organization`"
                . " WHERE `" . self::$tableName . "`.organization_id=`mailer_organization`.`id`"
                . " AND `mailer_organization`.`resource_id`=" . $resourceId 
                . " AND `" . self::$tableName . "`.`email`=''";
        DBController::getInstance()->query($sql);
    }
    
    public static function getContactsIsNotSend($emailId, $limit = 10) {
        $sql = "SELECT mc.* FROM mailer_email_resource as mer"
                . " INNER JOIN mailer_resource as mr ON mer.resource_id=mr.id"
                . " INNER JOIN mailer_organization as mo ON mr.id=mo.resource_id"
                . " INNER JOIN " . self::$tableName . " as mc ON mo.id=mc.organization_id"
                . " LEFT JOIN mailer_email_contact as mec ON mc.id=mec.contact_id"
                . " WHERE mer.email_id=$emailId AND mc.done=1 AND mc.id NOT IN"
                . " (SELECT contact_id FROM mailer_email_contact WHERE email_id=$emailId AND done=1)"
                . " ORDER BY mc.id ASC"
                . " LIMIT 0, $limit";

        $result = DBController::getInstance()->query($sql);
        
        $listContacts = array();
        foreach ($result->rows as $contact) {
            $mailerContact = new MailerContact();
            $mailerContact->mappingFromArray($contact);
            $listContacts[] = $mailerContact;
        }
        
        return $listContacts;
   }
    
    public static function getCountByEmail($emailId) {
        $sql = "SELECT count(*) as count FROM mailer_email_resource as mer"
                . " INNER JOIN mailer_resource as mr ON mer.resource_id=mr.id"
                . " INNER JOIN mailer_organization as mo ON mr.id=mo.resource_id"
                . " INNER JOIN " . self::$tableName . " as mc ON mo.id=mc.organization_id"
                . " WHERE mer.email_id=$emailId AND mc.done=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
   }
    
   public static function getCountEmailsByEmailId($emailId) {
        $sql = "SELECT SUM(LENGTH(mc.email) - LENGTH(REPLACE(mc.email, '@', ''))) as count"
                . " FROM mailer_email_resource as mer"
                . " INNER JOIN mailer_resource as mr ON mer.resource_id=mr.id"
                . " INNER JOIN mailer_organization as mo ON mr.id=mo.resource_id"
                . " INNER JOIN " . self::$tableName . " as mc ON mo.id=mc.organization_id"
                . " WHERE mer.email_id=$emailId AND mc.done=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
   }
 
    public static function deleteByResource($resourceId) {
        $sql = "DELETE FROM `" . self::$tableName . "`"
                . " USING `" . self::$tableName . "`, `mailer_organization`"
                . " WHERE `" . self::$tableName . "`.organization_id=`mailer_organization`.`id`"
                . " AND `mailer_organization`.`resource_id`=" . $resourceId;
        DBController::getInstance()->query($sql);
    }
   
}