<?php
require_once('model.php');

class SpiderManufacturer extends Model {
    private static $tableName = "spider_manufacturer";
    
    public $id = 0;
    public $name;
    public $link;
    public $site_manufacturer_id;
    public $script;
    public $done = 0;
    public $created_at;
    public $updated_at;
    
    public function __construct($id = 0) {
        parent::__construct();
        
        $this->listRows = array(
        'id', 
        'name', 
        'link',
        'site_manufacturer_id',
        'script',
        'done',
        'created_at',
        'updated_at');
        
        if ($id != 0 && $id != "" && !empty($id)) {
            $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `id`=" . $id;
            $manufacturer = DBController::getInstance()->query($sql);
            if (count($manufacturer->row) > 0) {
                $this->mappingFromArray($manufacturer->row);
            }
        }
    }
    
    public static function getManufacturers() {
        $sql = "SELECT * FROM `" . self::$tableName . "` ORDER BY `updated_at` DESC, `id` ASC";
        $result = DBController::getInstance()->query($sql);
        
        $listManufacturers = array();
        foreach ($result->rows as $manufacturer) {
            $spiderManufacturer = new SpiderManufacturer();
            $spiderManufacturer->mappingFromArray($manufacturer);
            $listManufacturers[] = $spiderManufacturer;
        }
        
        return $listManufacturers;
    }
    
    public function create() {
        $sql = "INSERT INTO `" . self::$tableName . "` SET ";

        $dateNow = $this->getDateNow();
        
        $this->created_at = $dateNow;
        $this->updated_at = $dateNow;
        
        $sql = $this->mappingSQL($sql);
        DBController::getInstance()->query($sql);

        $this->id = DBController::getInstance()->getLastId();
    }

    public function update() {
        $sql = "UPDATE `" . self::$tableName . "` SET ";

        $this->updated_at = $this->getDateNow();
        $sql = $this->mappingSQL($sql);

        $sql .= " WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public function delete() {
        SpiderCategory::deleteByManufacturer($this->id);
        
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
}