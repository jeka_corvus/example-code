<?php
require_once('resource/resource.base.php');

class ResourceClass extends ResourceBase {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function updateOrganizations() {
        $site = $this->mailerResource->uri . $this->mailerResource->uri_next_organization;
        $uri = Utility::encodeUri($site);
        $content = $this->browser->go($uri);
        
        echo "<pre>";
        print_r($site);
        echo "<br />";
        print_r($uri);
        echo "<br />";
        print_r($content);
        echo "</pre>" . PHP_EOL;
//        die;

        $redirectText = "Found. Redirecting to ";
        if (strpos($content, $redirectText) === 0) {
            $this->mailerResource->uri_next_organization = substr($content, strlen($redirectText));
            $this->mailerResource->update();
            return;
        }
        
        
        $this->dom = $this->browser->getDOM();

        // Get links to organizations
        $count = $this->getOrganizationsFromHTML($this->dom);

        // Set next organization
        $listParams = explode("&", $this->mailerResource->uri_next_organization);
        $newUri = "";
        $nextNumPage = 0;
//        "/epz/order/extendedsearch/results.html?searchString=&morphology=on&pageNumber=21714&sortDirection=false&recordsPerPage=_10&showLotsInfoHidden=false&fz44=on&fz223=on&priceFrom=0&priceTo=200000000000&currencyId=1&districts=5277317&regions=5277335%2C+5277327&af=true&ca=true&sortBy=RELEVANCE&openMode=USE_DEFAULT_PARAMS";
        if ($count > 0) {
            foreach ($listParams as $param) {
                if (stripos($param, "pageNumber=") !== false) {
                    $numPage = explode("=", $param);
                    $nextNumPage = intval($numPage[1]) + 1;
                    $newUri = str_replace($param, "pageNumber=" . $nextNumPage, 
                            $this->mailerResource->uri_next_organization);
                    break;
                }
            }
        }
        
        $this->mailerResource->uri_next_organization = $newUri;
        $this->mailerResource->update();
    }
    
    private function getOrganizationsFromHTML($dom) {
        $count = 0;
        $blocks = $dom->find('div [class="registerBox registerBoxBank margBtm20"] td[class=descriptTenderTd]');
        foreach($blocks as $element) {
            ++$count;
            $mailerOrganization = new MailerOrganization();
            $mailerOrganization->resource_id = $this->mailerResource->id;
            $mailerOrganization->name = trim($element->find("dd[class=nameOrganization] a", 0)->plaintext);
            $mailerOrganization->uri = $element->find("dt a", 0)->href;
            if (empty($mailerOrganization->name) || empty($mailerOrganization->uri)) {
                continue;
            }
//            if (!$mailerOrganization->isExist()) {
            $mailerOrganization->create();
//            }
        }
        return $count;
    }
    
    public function updateContacts() {
        $site = $this->mailerOrganization->uri;
        if (stripos($this->mailerOrganization->uri, $this->mailerResource->uri) === false) {
            $site = $this->mailerResource->uri . $this->mailerOrganization->uri;
        }
        $uri = Utility::encodeUri($site);
        $content = $this->browser->go($uri);
        
        echo "<pre>";
        print_r($site);
        echo "<br />";
        print_r($uri);
        echo "<br />";
        print_r($content);
        echo "</pre>" . PHP_EOL;
//        die;

        $foundRedirecting = "Found. Redirecting to ";
        $movedPermanently = "Moved Permanently. Redirecting to ";
        if (strpos($content, $foundRedirecting) === 0) {
            $this->mailerOrganization->uri = substr($content, strlen($foundRedirecting));
            $this->mailerOrganization->update();
            return;
        } else if (strpos($content, $movedPermanently) === 0) {
            $this->mailerOrganization->uri = substr($content, strlen($movedPermanently));
            $this->mailerOrganization->update();
            return;
        }
        
        $this->dom = $this->browser->getDOM();
        
        // Get blocks with params organization
        $blockOrganization = $this->dom->find('div [class=wrapper]', 0);
        
        $this->getContactsFromHTML($blockOrganization);
    }
    
    private function getContactsFromHTML($dom) {
        $mailerContact = new MailerContact();
        $mailerContact->organization_id = $this->mailerOrganization->id;
        
        $tbodyBlocks = $dom->find('div [class="noticeTabBoxWrapper"] table tbody');
        $isFind = false;
        foreach($tbodyBlocks as $tbody) {
            if ($isFind) {
                break;
            }

            $trBlocks = $tbody->find('tr');
            foreach($trBlocks as $tr) {
                $info = $tr->find("td", 0);
                if (!$isFind 
                    && $info->plaintext != "Организация" 
                    && $info->plaintext != "Наименование организации" 
                    && $info->plaintext != "Организация, осуществляющая размещение") {
                    break;
                }
                $isFind = true;
                
                if ($info->plaintext == "Организация" 
                    || $info->plaintext == "Наименование организации" 
                    || $info->plaintext == "Организация, осуществляющая размещение") {
                   $info = $tr->find("td", 1);
                   $mailerContact->title = trim($info->plaintext);
                   continue;
                }
                
                if ($info->plaintext == "Ответственное должностное лицо" 
                    || $info->plaintext == "Контактное лицо") {
                   $info = $tr->find("td", 1);
                   $mailerContact->name = trim($info->plaintext);
                   continue;
                }
                
                if ($info->plaintext == "Адрес электронной почты" 
                    || $info->plaintext == "Электронная почта") {
                   $info = $tr->find("td", 1);
                   $email = trim($info->plaintext);
                   if ($info->plaintext != "" && !$mailerContact->isExistEmail($email)) {
                        $mailerContact->email = "|" . $email . "|";
                   }
                   continue;
                }
                
                if ($info->plaintext == "Телефон" 
                    || $info->plaintext == "Номер контактного телефона") {
                   $info = $tr->find("td", 1);
                   if ($info->plaintext != "") {
                        $mailerContact->phone = trim($info->plaintext);
                   }
                   continue;
                }
            }
            
        }
//        echo "<pre>";
//        print_r($mailerContact);
//        echo "<br />";
//        print_r();
//        echo "</pre>" . PHP_EOL;
//        die;
        
        $mailerContact->done = 1;
        $mailerContact->emails_done = 1;
//        if (!$mailerContact->isExist($this->mailerResource->id)) {
            $mailerContact->create();
//        }
//        echo "<pre>";
//        print_r($mailerContact);
//        echo "<br />";
//        print_r();
//        echo "</pre>" . PHP_EOL;
//        die;
        
        $this->mailerOrganization->done = 1;
        $this->mailerOrganization->update();
    }
}