<?php
require_once('vendors/parser/simple_html_dom.php');
require_once('utility.php');

class DefaultConfig {
    const USER_AGENT            = "Mozilla/5.0 (Windows NT 5.2; WOW64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1";
    const VERBOSITY             = 1; // 0 - minimum; 1 - normal; 2 - maximum; 3 - full debug
    const CONNECTTIMEOUT        = 60;
    const COOKIE_FILE           = "cookies/cookie_file.txt";
    const PROXY_ENABLE          = false;
    const PROXY                 = "";//193.200.32.233:8080";
}

if (!class_exists('ConfigSpider')) {
    class ConfigSpider extends DefaultConfig {
        public static $rootDir = __DIR__;
        public static $folderToImages = 'images/';
        public static $folderToImagesSite1 = 'image/';
        public static $folderToImagesSite2 = 'data/';
        
        public static $metaDescriptionProduct = 'Торговое, холодильное, пищевое оборудование, металлическая мебель. Комплектация  предприятий торговли и общепита, объектов строительства. Продажа, доставка, монтаж, обслуживание.';
        
        public static function getPathToImages() {
            return self::$rootDir . "/" . self::$folderToImages;
        }
    }
}

