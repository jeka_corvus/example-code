<?php
require_once('vendors/parser/simple_html_dom.php');
require_once('utility/utility.php');

class DefaultConfig {
    const USER_AGENT            = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0";
    const VERBOSITY             = 1; // 0 - minimum; 1 - normal; 2 - maximum; 3 - full debug
    const CONNECTTIMEOUT        = 60;
    const COOKIE_FILE           = "cookies/cookie_file.txt";
    const PROXY_ENABLE          = false;
    const PROXY                 = "52.29.226.246:1992";//""
}

if (!class_exists('ConfigMailer')) {
    class ConfigMailer extends DefaultConfig {
        public static $rootDir = __DIR__;
        public static $dirImagesEmail = "/images/email/";
        public static $countContactsSend = 10;
    }
}

