<?php
class Browser {
    public  $postfixCookieFile = '';
    private $verbosity = ConfigSpider::VERBOSITY;
    private $currentURI = '';
    private $dom;
    private $lastRequestInfo = array();

    public function __construct() {
        $this->postfixCookieFile = '.' . time() . '.' . mt_rand(0, 1000);
    }

    public function setVerbosity($value) {
        $this->verbosity = $value;
    }

    public function getDOM() {
        return $this->dom;
    }

    public function getCurrentURL() {
        return $this->currentURI;
    }
    
    public function go($uri, $withDom = TRUE) {
        $content = $this->request($uri);
        if ($withDom) {
            $this->dom = str_get_html($content);
        }
        $this->currentURI = $uri;
        return $content;
    }

    public function post($uri, $data) {
        $content = $this->request($uri, $data);
        $this->dom = str_get_html($content);
        $this->currentURI = $uri;
        return $content;
    }

    public function ajax($uri, $data = array()) {
        $content = $this->request($uri, $data);
        $arr = explode('|', $content);
        // todo if error returns
        $cnt = count($arr);
        for($i=0; $i<$cnt; $i+=4) {
            //echo @$arr[$i+1] . ':' . @$arr[$i+2] . ':' . @$arr[$i+3] . PHP_EOL;
            if (@$arr[$i+1] == 'updatePanel') {
                $this->dom->find('div[id=' . $arr[$i+2] . ']', 0)->innertext = $arr[$i+3];
            }
            if (@$arr[$i+1] == 'hiddenField') {
                $this->dom->find('input[id=' . $arr[$i+2] . ']', 0)->attr['value'] = $arr[$i+3];
            }
        }
        $this->dom = str_get_html($this->dom->save());
        return $content;
    }

    public function download($uri, $filename) {
        $content = $this->request($uri);
        $fp = fopen($filename, 'w');
        if ($fp === FALSE) {
            throw new Exception("Failed to open $filename for writing.");
        }
        $ret = fwrite($fp, $content);
        if ($ret === FALSE) {
            throw new Exception("Failed to write content to $filename.");
        }
        fclose($fp);
    }

    private function request($uri, $data = array()) {
        if ($this->verbosity > 0) {
            $method = ($data) ? 'POST' : 'GET';
            echo "Sending request $method $uri..." . PHP_EOL;
        }
        $ch = curl_init();
        if ($ch === FALSE) {
            throw new Curl_Init_Exception;
        }
        $options = array(
            CURLOPT_URL => $uri,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => ConfigSpider::CONNECTTIMEOUT, // todo
            CURLOPT_USERAGENT => ConfigSpider::USER_AGENT, // todo
            CURLOPT_COOKIEFILE => ConfigSpider::COOKIE_FILE . $this->postfixCookieFile, // todo
            CURLOPT_COOKIEJAR => ConfigSpider::COOKIE_FILE . $this->postfixCookieFile, // todo
            CURLOPT_REFERER => $this->currentURI,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLINFO_HEADER_OUT => ($this->verbosity >= 3) ? TRUE : FALSE,
            CURLOPT_NOPROGRESS => ($this->verbosity >= 2) ? FALSE : TRUE,
            CURLOPT_PROGRESSFUNCTION => ($this->verbosity >= 2) ? array($this, 'progress') : NULL,
        );
        if (ConfigSpider::PROXY_ENABLE) { // todo
            $options[CURLOPT_PROXY] = ConfigSpider::PROXY;
        }
        if ($data) {
            $fields = array();
            foreach($data as $key => $value) {
                $fields[] = urlencode($key) . '=' . urlencode($value);
            }
            $options[CURLOPT_POSTFIELDS] = implode('&', $fields);
        }
        $ret = curl_setopt_array($ch, $options);
        if ($ret === FALSE) {
            throw new Curl_Setopt_Exception('Reason: ' . curl_error($ch));
        }
        $content = curl_exec($ch);
        echo PHP_EOL;
        $this->lastRequestInfo = curl_getinfo($ch);
        if ($content === FALSE) {
            throw new Curl_Exec_Exception(curl_error($ch), curl_errno($ch));
        } else {
            if ($this->verbosity >= 2) {
                echo "Request info: ";
                print_r($this->lastRequestInfo);
            }
            if ($this->verbosity >= 1) {
                echo "Request OK." . PHP_EOL;
            }
        }
        curl_close($ch);
        return $content;
    }

    function progress($dlTotal, $dlNow, $upTotal, $upNow) {
        $dlPercent = ($dlTotal) ? round(100*$dlNow/$dlTotal) : '-';
        $upPercent = ($upTotal) ? round(100*$upNow/$upTotal) : '-';
        echo "($dlPercent%,$upPercent%)";
    }
}

class Curl_Init_Exception extends Exception {
}

class Curl_Setopt_Exception extends Exception {
}

class Curl_Exec_Exception extends Exception {
}
