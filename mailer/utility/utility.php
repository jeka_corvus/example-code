<?php

class Utility {

    public static function encodeUri($uri) {
        $arrayReplaceFrom = array('%2F',    '%3A',  '%3F',  '%3D',  '%25',  '%26',  '%2B',  '%23');
        $arrayReplaceTo =   array('/',      ':',    '?',    '=',    '%',    '&',    '+',    '#');
        $newUri = str_replace($arrayReplaceFrom, $arrayReplaceTo, rawurlencode($uri));
        
        return $newUri;
    }

    // функция создания ссылки
    public static function renderUri($params) {
        $uri = "./";
        $uri .= $params['script'];
        
        if (isset($params['args']) && count($params['args']) > 0) {
            $uri .= "?";
            $pairs = array();
            foreach ($params['args'] as $key => $value) {
                $pairs[] = $key . '=' . $value;
            }
            $uri .= join('&', $pairs);
        }
        
        return $uri;
    }
    
    // функция превода текста с кириллицы в траскрипт
    public static function encodeStringToTranslit($str) {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 
                    'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 
                    'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 
                    'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
                    , ' ', '/', '\\', '"', '\'', '?', '&', '.', ',', '!', ')', '(');
        
        $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 
            'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 
            'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 
            's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya'
            , '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
        
        return str_replace($rus, $lat, $str);
    }
    
    // функция превода текста с кириллицы в траскрипт в нижнем регистре
    public static function encodeLowerStringToTranslit($str) {
        return strtolower(self::encodeStringToTranslit($str));
    }
    
    // функция превода текста с кириллицы в траскрипт в нижнем регистре и удаление в конце строки всех '-'
    // чтобы строка оканчивалась на букву либо цифру
    public static function encodeLowerStringToTranslitTrim($str) {
        $result = self::encodeLowerStringToTranslit($str);
        
        do {
            $pos = stripos($result, '-');
            if ($pos === false || $pos != 0) {
                break;
            }
            $result = substr($result, 1);
        } while(1 == 1);
        
        do {
            $pos = strripos($result, '-');
            if ($pos === false || $pos != strlen($result) - 1) {
                break;
            }
            $result = substr($result, 0, strlen($result) - 1);
        } while(1 == 1);

        return $result;
    }
    
}
