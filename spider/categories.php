<?php
require_once('default.conf.php');
require_once('db.controller.php');

if (isset($_GET['manufacturer_id'])) {
    $manufacturerGeneral = new SpiderManufacturer($_GET['manufacturer_id']);
} else {
    echo "В адресной строке нет ID производителя!!!";
    die;
}
if (isset($_POST['id'])) {
    if ($_POST['name'] != "" && $_POST['link'] != "") {
        $spiderCategory = new SpiderCategory($_POST['id']);
        $spiderCategory->name                       = $_POST['name'];
        $spiderCategory->link                       = $_POST['link'];
        $spiderCategory->site_parent_category_id    = $_POST['site_parent_category_id'];
        $spiderCategory->site_category_id           = $_POST['site_category_id'];
        $spiderCategory->done                       = isset($_POST['done']) ? 1 : 0;
                
        $spiderCategory->update();
        
        header("Location: ./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$spiderCategory->parent_id");
    }
}

if (isset($_GET['action'])) {
    $actionCategory = new SpiderCategory($_GET['category_id']);
    
    switch ($_GET['action']){
        case "edit":
            break;
        
        case "delete":
            $actionCategory->delete();
            header("Location: ./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$_GET[id]");
            break;
        
        case "update_products":
            if (!empty($manufacturerGeneral) && !empty($manufacturerGeneral->script)) {
                require_once('manufacturer/' . $manufacturerGeneral->script);
                $manufacturerObject = new ManufacturerClass();
                $manufacturerObject->setSpiderManufacturer($manufacturerGeneral);
                $manufacturerObject->setSpiderCategory($actionCategory);
                $manufacturerObject->updateProducts();
                $actionCategory->update();
                header("Location: ./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$actionCategory->parent_id");
            }
            break;

        case "synchronization":
            if ($actionCategory->site_parent_category_id == 0) {
                echo "<script type='text/javascript'>alert('Не заполнен \'ID родительской категории на сайте\'!!!');</script>";
                unset($actionCategory);
            } else {
                $actionCategory->synchronizationWithSite();
                header("Location: ./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$actionCategory->parent_id");            
            }
            break;

        case "empty_id_site":
            $actionCategory->setEmptySiteIdAndParent();
            header("Location: ./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$actionCategory->parent_id");
            break;

        default:
            header("Location: ./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$actionCategory->parent_id");
            break;
    }
}

?>
<html>
    <head>
        <style>
           table { 
            border: 4px double black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
           }
           th { 
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           td { 
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           .dark {
                background: #ccc; /* Цвет фона ячеек */
           }
           textarea {
               width: 800px;
               height: 150px;
           }
           input[type=text] {
                width: 800px;
           } 
        </style>          
    </head>
    <body>

    <?php if (isset($actionCategory)) { ?>
        <h2>Редактировать категорию</h2>
        <form action="<?php echo "./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$actionCategory->parent_id"?>" name="edit_category" method="post">
            <table>
                <tr>
                    <th><b>ID</b></th>
                    <td>
                        <?php echo isset($actionCategory) ? $actionCategory->id : ""; ?>
                        <input type="hidden" name="id" value="<?php echo isset($actionCategory) ? $actionCategory->id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Родительский каталог ID</b></th>
                    <td>
                        <?php echo isset($actionCategory) ? $actionCategory->parent_id : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Производитель ID</b></th>
                    <td>
                        <?php echo isset($actionCategory) ? $actionCategory->manufacturer_id : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Название</b></th>
                    <td>
                        <input type="text" name="name" value="<?php echo isset($actionCategory) ? $actionCategory->name : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Ссылка</b></th>
                    <td>
                        <input type="text" name="link" value="<?php echo isset($actionCategory) ? $actionCategory->link : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>ID родительской категории на сайте</b></th>
                    <td>
                        <input type="text" name="site_parent_category_id" value="<?php echo isset($actionCategory) ? $actionCategory->site_parent_category_id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>ID категории на сайте</b></th>
                    <td>
                        <input type="text" name="site_category_id" value="<?php echo isset($actionCategory) ? $actionCategory->site_category_id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Закончен</b></th>
                    <td>
                        <input type="checkbox" name="done" <?php echo (isset($actionCategory) && $actionCategory->done) ? "checked" : ""; ?> />
                    </td>
                </tr>
                <tr>
                    <th><b>Дата создания</b></th>
                    <td>
                        <?php echo isset($actionCategory) ? $actionCategory->created_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Дата обновления</b></th>
                    <td>
                        <?php echo isset($actionCategory) ? $actionCategory->updated_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <button type="submit">Отправить</button>
                        </form>
                        &nbsp;&nbsp;&nbsp;<a href="<?php echo "./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$actionCategory->parent_id" ?>">Отмена</a>
                    </td>
                </tr>
            </table>        
    <?php
    }
    ?>
        
        <?php 
            $backLink = "";
            if (isset($_GET['id']) && !empty($_GET['id'])) {
                $idCategory = $_GET['id'];
                do {
                    $categoryBack = new SpiderCategory($idCategory);
                    $backLink = "-><a href='categories.php?manufacturer_id=$manufacturerGeneral->id&id=$categoryBack->parent_id'>"
                        . "$categoryBack->name</a>" . $backLink;
                    $idCategory = $categoryBack->parent_id;
                } while ($idCategory != 0);
            } 

            $parentId = 0;
            if (isset($_GET['id'])) {
                $parentId = $_GET['id'];
            }
        ?>
    
        <h2>Статистика</h2>
        <table>
            <tr>
                <th><b>Всех категорий</b></th>
                <th><b>Готовых категорий</b></th>
                <th><b>Незаконченых категорий</b></th>
            </tr>
            <tr>
                <td><b><?php echo $countCategoriesAll = SpiderCategory::getCountByManufacturerAndParent($manufacturerGeneral->id, $parentId) ?></b></td>
                <td style='color: green'><b><?php echo $countCategoriesIsDone = SpiderCategory::getCountByManufacturerAndParentIsDone($manufacturerGeneral->id, $parentId) ?></b></td>
                <td style='color: red'><b><?php echo $countCategoriesAll - $countCategoriesIsDone ?></b></td>
            </tr>
        </table>

        <br/>
        <h2><a href="./"><?php echo $manufacturerGeneral->name; ?></a><?php echo $backLink ?></a></h2>
        <h2>Список категории</h2>
        <table>
            <tr>
                <th><b>#</b></th>
                <th><b>ID</b></th>
                <th><b>Родительский каталог ID</b></th>
                <th><b>Производитель ID</b></th>
                <th><b>Название</b></th>
                <th><b>Ссылка</b></th>
                <th><b>ID родительской категории на сайте</b></th>
                <th><b>ID категории на сайте</b></th>
                <th><b>Закончен</b></th>
                <th><b>Дата создания</b></th>
                <th><b>Дата обновления</b></th>
                <th><b>Действия</b></th>
            </tr>
<?php             
        $categories = SpiderCategory::getCategories($manufacturerGeneral->id, $parentId);
        
        foreach ($categories as $key => $category) {
            $outText = "<tr " . (($key % 2) ? "class='dark'" : "") . ">"
                . "<td>" . ($key + 1) . "</td>";

            $countProducts = SpiderProduct::getCountByCategory($category->id);
            if ($countProducts > 0) {
                $outText .= "<td><a href=\"./products.php?category_id=$category->id\">$category->id</a></td>";
                $countProductsIsDone = SpiderProduct::getCountByCategoryIsDone($category->id);
                if ($countProducts == $countProductsIsDone && $category->done == 0) {
                    $category->done = 1;
                    $category->update();
                } else if ($countProducts != $countProductsIsDone && $category->done == 1) {
                    $category->done = 0;
                    $category->update();
                }
            } else{
                $outText .= "<td>$category->id</td>";
            }

            $outText .= "<td>$category->parent_id</td>"
                . "<td>$category->manufacturer_id</td>";
            
            $countCategories = SpiderCategory::getCountByParent($category->id);
            if ($countCategories > 0) {
                $outText .= "<td><a href=\"./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$category->id\">$category->name</a></td>";
                $countCategoriesIsDone = SpiderCategory::getCountByParentIsDone($category->id);
                if ($countCategories == $countCategoriesIsDone && $category->done == 0) {
                    $category->done = 1;
                    $category->update();
                } else if ($countCategories != $countCategoriesIsDone && $category->done == 1) {
                    $category->done = 0;
                    $category->update();
                }
            } else{
                $outText .= "<td>$category->name</td>";
            }
            $outText .= "<td><a href='$manufacturerGeneral->link/$category->link' target='_blank'>Ссылка на сайт производителя</a></td>"
                . "<td><b style='color:" . ($category->site_parent_category_id > 0 ? "green" : "red") . "'>$category->site_parent_category_id</b></td>"
                . "<td>$category->site_category_id</td>"
                . "<td><b style='color:" . ($category->done ? "green'>Да" : "red'>Нет") . "</b></td>"
                . "<td>$category->created_at</td>"
                . "<td>$category->updated_at</td>"
                . "<td><table style='border: 0px;'><tr>"
                    . "<td><a href='./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$parentId&category_id=$category->id&action=delete' onclick=\"return confirm('Вы уверены, что хотите удалить - \'$category->name\'?')\">Удалить</a></td>"
                    . "<td><a href='./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$parentId&category_id=$category->id&action=edit'>Редактировать</a></td>"
                    . "<td><a href='./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$parentId&category_id=$category->id&action=empty_id_site' onclick=\"return confirm('Вы уверены, что хотите обнулить ID на сайте и родительской категории - \'$category->name\'?')\">Обнулить ID на сайте</a></td>";
            
                if ($countCategories == 0) {
                    $outText .= "<td><a href='./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$parentId&category_id=$category->id&action=update_products' onclick=\"return confirm('Вы уверены, что хотите обновить - \'$category->name\'?')\">Обновить товары</a></td>";
                }
                $outText .= "<td><a href='./categories.php?manufacturer_id=$manufacturerGeneral->id&id=$parentId&category_id=$category->id&action=synchronization' onclick=\"return confirm('Вы уверены, что хотите синхронизировать с сайтом - \'$category->name\'?')\">Синхронизация</a></td>"
                        ."</tr></table></td>"
            . "</tr>";
            echo $outText;
        }
?>
        </table>
    </body>    
</html>