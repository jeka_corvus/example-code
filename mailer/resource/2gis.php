<?php
require_once('resource/resource.base.php');

class ResourceClass extends ResourceBase {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function updateOrganizations() {
        $site = $this->mailerResource->uri . $this->mailerResource->uri_next_organization;
        $uri = Utility::encodeUri($site);
        $content = $this->browser->go($uri);

        echo "<pre>";
        print_r($site);
        echo "<br />";
        print_r($uri);
        echo "<br />";
        print_r($content);
        echo "</pre>" . PHP_EOL;
//        die;

        $redirectText = "Found. Redirecting to ";
        if (strpos($content, $redirectText) === 0) {
            $this->mailerResource->uri_next_organization = substr($content, strlen($redirectText));
            $this->mailerResource->update();
            return;
        }
        
        
        $this->dom = $this->browser->getDOM();

        // Get links to organizations
        $blockOrganizations = $this->dom->find('div[class=searchResults__list]', 0);
        if ($blockOrganizations == null) {
            $blockOrganizations = $this->dom->find('div[class=mediaResults__results]', 0);
        }
        $this->getOrganizationsFromHTML($blockOrganizations);

        // Set next organization
        $pagination = $this->dom->find('nav[class=pagination__pages]', 0);
        $getHref = false;
        $this->mailerResource->uri_next_organization = "";
        foreach($pagination->children() as $element) {
            if ($getHref) {
                $this->mailerResource->uri_next_organization = $element->href;
                break;
            }
            
            if ($element->class == "pagination__page _current") {
                $getHref = true;
            }
        }
        $this->mailerResource->update();
    }
    
    private function getOrganizationsFromHTML($dom) {
        $blocks = $dom->find('article');
        foreach($blocks as $element) {
            $mailerOrganization = new MailerOrganization();
            $mailerOrganization->resource_id = $this->mailerResource->id;
            $title = $element->find("h3[class=mediaMiniCard__name]", 0);
            if ($title == null) {
                $title = $element->find("a", 0);
            }
            $mailerOrganization->name = trim($title->plaintext);
            $mailerOrganization->uri = $element->find("a", 0)->href;
            if (empty($mailerOrganization->name) || empty($mailerOrganization->uri)) {
                continue;
            }
            if (!$mailerOrganization->isExist()) {
                $mailerOrganization->create();
            }
        }
    }
    
    public function updateContacts() {
        $site = $this->mailerResource->uri . $this->mailerOrganization->uri;
        $uri = Utility::encodeUri($site);
        $content = $this->browser->go($uri);
        
        echo "<pre>";
        print_r($site);
        echo "<br />";
        print_r($uri);
        echo "<br />";
        print_r($content);
        echo "</pre>" . PHP_EOL;
//        die;

        $foundRedirecting = "Found. Redirecting to ";
        $movedPermanently = "Moved Permanently. Redirecting to ";
        if (strpos($content, $foundRedirecting) === 0) {
            $this->mailerOrganization->uri = substr($content, strlen($foundRedirecting));
            $this->mailerOrganization->update();
            return;
        } else if (strpos($content, $movedPermanently) === 0) {
            $this->mailerOrganization->uri = substr($content, strlen($movedPermanently));
            $this->mailerOrganization->update();
            return;
        }
        
        $this->dom = $this->browser->getDOM();
        
        // Get blocks with params organization
        $blockOrganization = $this->dom->find('div[class=frame__content]', 0);
        
        $this->getContactsFromHTML($blockOrganization);
    }
    
    private function getContactsFromHTML($dom) {
        $mailerContact = new MailerContact();
        $mailerContact->organization_id = $this->mailerOrganization->id;
        
        $title = $dom->find("h1[class=mediaCardHeader__cardHeaderName]", 0);
        if ($title == null) {
            $title = $dom->find("h1[class=cardHeader__headerNameText]", 0);
        }
        $mailerContact->title = $title->plaintext;
        
        $blocksSite = $dom->find('li[class="mediaContacts__groupItem _website"]');
        if ($blocksSite == null) {
            $blocksSite = $dom->find('div[class="contact__link _type_website"]');
        }
        foreach($blocksSite as $element) {
            $siteItem = $element->find('a[class="mediaContacts__item mediaContacts__website"]', 0);
            if ($siteItem == null) {
                $siteItem = $element->find('a[class="link contact__linkText"]', 0);
            }
            $site = $siteItem->plaintext;
            $mailerContact->site .= ($mailerContact->site == "") ? "" : ";";
            $mailerContact->site .= "http://" . $site;
        }
        
        $blocksPhone = $dom->find('li[class="mediaContacts__groupItem _phone"]');
        if ($blocksPhone == null) {
            $blocksPhone = $dom->find('div[class="contact__phonesItem _type_phone"]');
        }
        foreach($blocksPhone as $element) {
            $phoneItem = $element->find("a[class='mediaContacts__phonesNumber']", 0);
            if ($phoneItem == null) {
                $phoneItem = $element->find('a span[class="contact__phonesItemLinkNumber"]', 0);
            }
            if ($phoneItem != null) {
                $mailerContact->phone .= ($mailerContact->phone == "") ? "" : ";";
                $mailerContact->phone .= $phoneItem->plaintext;
            }
        }
        
        if (!$mailerContact->isExist($this->mailerResource->id)) {
            $mailerContact->create();
        }
        
        $blocksEmail = $dom->find('li[class="mediaContacts__groupItem _email"]');
        if ($blocksEmail != null) {
            foreach($blocksEmail as $element) {
                $email = $element->find("a[class='mediaContacts__item mediaContacts__email']", 0)->plaintext;
                if ($mailerContact->isExistEmail($email)) {
                    continue;
                }
                $mailerContact->email .= ($mailerContact->email == "") ? "|" : "";
                $mailerContact->email .= $email . "|";
            }
        }
        
        $mailerContact->done = 1;
        $mailerContact->update();
        
//        echo "<pre>";
//        print_r($mailerContact);
//        echo "<br />";
//        print_r();
//        echo "</pre>" . PHP_EOL;
//        die;
        
        $this->mailerOrganization->done = 1;
        $this->mailerOrganization->update();
    }
}