<?php
require_once('model.php');

class MailerEmailContact extends Model {
    private static $tableName = "mailer_email_contact";
    
    public $id = 0;
    public $email_id;
    public $contact_id;
    public $list_emails;
    public $done = 0;
    public $created_at;
    
    public function __construct($id = 0) {
        parent::__construct();
        
        $this->listRows = array(
        'id', 
        'email_id', 
        'contact_id',
        'list_emails',
        'done',
        'created_at');
        
        if ($id != 0 && $id != "" && !empty($id)) {
            $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `id`=" . $id;
            $result = DBController::getInstance()->query($sql);
            if (count($result->row) > 0) {
                $this->mappingFromArray($result->row);
            }
        }
    }
    
    public function create() {
        $sql = "INSERT INTO `" . self::$tableName . "` SET ";

        $dateNow = $this->getDateNow();
        
        $this->created_at = $dateNow;
        
        $sql = $this->mappingSQL($sql);
        DBController::getInstance()->query($sql);

        $this->id = DBController::getInstance()->getLastId();
    }

    public function update() {
        $sql = "UPDATE `" . self::$tableName . "` SET ";

        $sql = $this->mappingSQL($sql);

        $sql .= " WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public function createOrUpdate() {
        if ($this->id == 0) {
            $this->create();
        } else {
            $this->update();
        }
    }

    public function delete() {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }

    public static function clear($emailId) {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `email_id`=" . $emailId;
        DBController::getInstance()->query($sql);
    }
    
    public function getByEmailAndContact() {
        $sql = "SELECT * FROM `" . self::$tableName 
                . "` WHERE `email_id`=$this->email_id AND `contact_id`=$this->contact_id";
        $result = DBController::getInstance()->query($sql);

        if (count($result->row) > 0) {
            $this->mappingFromArray($result->row);
        }
    }
     
    public static function getCountByEmail($emailId) {
        $sql = "SELECT count(*) as count FROM " . self::$tableName
                . " WHERE email_id=$emailId AND done=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
   }
     
    public static function getCountEmailsByEmailId($emailId) {
        $sql = "SELECT SUM(LENGTH(list_emails) - LENGTH(REPLACE(list_emails, '@', ''))) as count"
                . " FROM " . self::$tableName
                . " WHERE email_id=$emailId AND done=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

   public static function deleteWithEmptyEmails($resourceId) {
        $sql = "DELETE FROM `" . self::$tableName . "`"
                . " USING `" . self::$tableName . "`,`mailer_contact`, `mailer_organization`"
                . " WHERE `" . self::$tableName . "`.contact_id=`mailer_contact`.`id`"
                . " AND `mailer_contact`.organization_id=`mailer_organization`.`id`"
                . " AND `mailer_organization`.`resource_id`=" . $resourceId 
                . " AND `mailer_contact`.`email`=''";
        DBController::getInstance()->query($sql);
    }
    
   
}