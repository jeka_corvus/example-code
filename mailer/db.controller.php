<?php
require_once('mysql.php');
// Configuration
require_once('config.php');
require_once('model/mailer.resource.php');
require_once('model/mailer.organization.php');
require_once('model/mailer.contact.php');
require_once('model/mailer.email.php');
require_once('model/mailer.email.resource.php');
require_once('model/mailer.email.contact.php');

class DBController {
    
    private $db;
    
    private static $instance = null;
    
    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    private function __clone() {}
    private function __construct() {
        $this->db = new MySQL(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);        
    }

    public function query($sql) {
        return $this->db->query($sql);
    }
    
    public function getLastId() {
        return $this->db->getLastId();
    }
    
    public function escape($str) {
        return $this->db->escape($str);
    }
    
}
