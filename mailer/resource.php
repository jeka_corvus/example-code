<?php
require_once('default.conf.php');
require_once('db.controller.php');
require_once('utility/paginator.php');

$pageParams = "";
if (isset($_GET['page'])) {
    $pageParams .= 'page=' . $_GET['page'];
}

if (isset($_POST['id'])) {
    if ($_POST['name'] != "" && $_POST['uri'] != "") {
        $mailerResource = new MailerResource($_POST['id']);
        $mailerResource->name                     = $_POST['name'];
        $mailerResource->uri                      = $_POST['uri'];
        $mailerResource->script                   = $_POST['script'];
        $mailerResource->uri_next_organization    = $_POST['uri_next_organization'];
        $mailerResource->done                     = isset($_POST['done']) ? 1 : 0;
                
        if ($_POST['id'] == "") {
            $mailerResource->create();
        } else {
            $mailerResource->update();
        }
        header('Location: ./resource.php' . ($pageParams == "" ? "" : "?") . $pageParams);
    }
}


if (isset($_GET['action'])) {
    $actionResource = new MailerResource($_GET['id']);
    
    switch ($_GET['action']){
        case "edit":
            break;
        
        case "delete":
            MailerContact::deleteByResource($actionResource->id);
            MailerOrganization::deleteByResource($actionResource->id);
            $actionResource->delete();
            header('Location: ./resource.php' . ($pageParams == "" ? "" : "?") . $pageParams);
            break;
        
        default:
            header('Location: ./resource.php' . ($pageParams == "" ? "" : "?") . $pageParams);
            break;
    }
}

?>
<html>
    <head>
        <title>Mailer Resource</title>
        <script type="text/javascript" src="./js/utility.js"></script>
        <style>
           table { 
            border: 4px double black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
           }
           th { 
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           td { 
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           .dark {
                background: #ccc; /* Цвет фона ячеек */
           }
           textarea {
               width: 800px;
               height: 150px;
           }
           input[type=text] {
                width: 800px;
           } 
        </style>          
    </head>
    <body>

        <h2><a href="#" onclick="showHideElement('block_add_edit'); return false;">Добавить\Редактировать ресурс</a></h2>
        <div id="block_add_edit" style="display: <?php echo isset($actionResource) ? "block" : "none"; ?>;">
            <form action="./resource.php<?php echo  ($pageParams == "" ? "" : "?") . $pageParams ?>" name="add_edit_resource" method="post">
                <table>
                    <tr>
                        <th><b>ID</b></th>
                        <td>
                            <?php echo isset($actionResource) ? $actionResource->id : ""; ?>
                            <input type="hidden" name="id" value="<?php echo isset($actionResource) ? $actionResource->id : ""; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th><b>Название</b></th>
                        <td>
                            <input type="text" name="name" value="<?php echo isset($actionResource) ? $actionResource->name : ""; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th><b>Ссылка</b></th>
                        <td>
                            <input type="text" name="uri" value="<?php echo isset($actionResource) ? $actionResource->uri : ""; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th><b>Скрипт</b></th>
                        <td>
                            <input type="text" name="script" value="<?php echo isset($actionResource) ? $actionResource->script : ""; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th><b>Следующий список организаций</b></th>
                        <td>
                            <input type="text" name="uri_next_organization" value="<?php echo isset($actionResource) ? $actionResource->uri_next_organization : ""; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th><b>Закончен</b></th>
                        <td>
                            <input type="checkbox" name="done" <?php echo (isset($actionResource) && $actionResource->done) ? "checked" : ""; ?> />
                        </td>
                    </tr>
                    <tr>
                        <th><b>Дата создания</b></th>
                        <td>
                            <?php echo isset($actionResource) ? $actionResource->created_at : ""; ?>
                        </td>
                    </tr>
                    <tr>
                        <th><b>Дата обновления</b></th>
                        <td>
                            <?php echo isset($actionResource) ? $actionResource->updated_at : ""; ?>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <button type="submit">Сохранить</button>
                            &nbsp;&nbsp;&nbsp;<a href='./resource.php<?php echo  ($pageParams == "" ? "" : "?") . $pageParams ?>'>Отмена</a>
                        </td>
                    </tr>
                </table>        
            </form>
        </div>
        <?php 
            $paginator = new Paginator();
            $paginator->countAllItems = MailerResource::getCount();
            $paginator->uri = "./resource.php?";
            $paginator->activePage = isset($_GET['page']) ? $_GET['page'] : 1;
            $contentPagination = $paginator->render();
        ?>

        <h2><a href="./">Home</a></h2>
        
        <h2>Список ресурсов</h2>
        <br />
        <?php echo $contentPagination; ?>
        <br />
        <table>
            <tr>
                <th><b>#</b></th>
                <th><b>ID</b></th>
                <th><b>Название</b></th>
                <th><b>Ссылка</b></th>
                <th><b>Скрипт</b></th>
                <th><b>Следующий список организаций</b></th>
                <th><b>Закончен</b></th>
                <th><b>Дата создания</b></th>
                <th><b>Дата обновления</b></th>
                <th><b>Действия</b></th>
            </tr>
<?php             
        $resources = MailerResource::getResources(($paginator->activePage - 1) * $paginator->countItemsToPage, 
                $paginator->countItemsToPage);
        
        $index = ($paginator->activePage - 1) * $paginator->countItemsToPage;
    	foreach ($resources as $key => $resource) {
            
            $outText = "<tr " . (($index % 2) ? "class='dark'" : "") . ">"
                . "<td>" . ($index++ + 1) . "</td>"
                . "<td>$resource->id</td>";
            
            $countOrganizations = MailerOrganization::getCountByResource($resource->id);
            $outText .= "<td><a href=\"./organization.php?resource_id=$resource->id\">$resource->name</a></td>";
            $countOrganizationsIsDone = MailerOrganization::getCountByResourceIsDone($resource->id);
            if ($countOrganizations != 0 && $countOrganizations == $countOrganizationsIsDone
                    && $resource->done == 0) {
                $resource->done = 1;
                $resource->update();
            } else if ($countOrganizations != 0 && $countOrganizations != $countOrganizationsIsDone
                    && $resource->done == 1) {
                $resource->done = 0;
                $resource->update();
            }

            $outText .= "<td><a href='$resource->uri' target='_blank'>Ссылка на ресурс</a></td>"
                . "<td>$resource->script</td>";
            if ($resource->uri_next_organization == "") {
                $outText .= "<td></td>";
            } else {
                $url = parse_url($resource->uri);
                $url_site = $url['scheme'] . '://' . $url['host'];
                $outText .= "<td><a href='$url_site$resource->uri_next_organization' target='_blank'>Ссылка на организацию</a></td>";
            }
            $outText .=  "<td><b style='color:" . ($resource->done ? "green'>Да" : "red'>Нет") . "</b></td>"
                . "<td>$resource->created_at</td>"
                . "<td>$resource->updated_at</td>"
                . "<td><table style='border: 0px;'><tr>"
                    . "<td><a href='./resource.php?action=delete&id=$resource->id" . ($pageParams == "" ? "" : "&") . $pageParams . "' onclick=\"return confirm('Вы уверены, что хотите удалить - $resource->name?')\">Удалить</a></td>"
                    . "<td><a href='./resource.php?action=edit&id=$resource->id" . ($pageParams == "" ? "" : "&") . $pageParams . "'>Редактировать</a></td>"
                . "</tr></table></td>"
            . "</tr>";
            
            echo $outText;
        }
?>
        </table>
        <br />
        <?php echo $contentPagination; ?>
    </body>    
</html>