<?php
require_once('browser.php');

class ManufacturerClass {
    
    private $browser;
    private $dom;
    private $spiderManufacturer;
    private $spiderCategory;
    private $spiderProduct;
    
    public function __construct() {
        $this->browser = new Browser();
    }

    public function setSpiderManufacturer($spiderManufacturer) {
        $this->spiderManufacturer = $spiderManufacturer;
    }

    public function setSpiderCategory($spiderCategory) {
        $this->spiderCategory = $spiderCategory;
    }

    public function setSpiderProduct($spiderProduct) {
        $this->spiderProduct = $spiderProduct;
    }

    public function updateCatalogs() {
        $this->browser->go($this->spiderManufacturer->link);
        $this->dom = $this->browser->getDOM();
        
        $blockCatalogs = $this->dom->find('div[id=accordion] ul', 0);
        $this->getCatalogFromHTML($blockCatalogs);
    }
    
    private function getCatalogFromHTML($dom, $parentId = 0) {
        foreach($dom->children() as $element) {
            $spiderCategory = new SpiderCategory();
            $spiderCategory->parent_id = $parentId;
            $spiderCategory->manufacturer_id = $this->spiderManufacturer->id;
            $spiderCategory->name = $element->find("a", 0)->plaintext;
            $spiderCategory->link = $element->find("a", 0)->href;
            if (!$spiderCategory->isExist()) {
                $spiderCategory->create();
            }
            
            $ulElement = $element->find('ul', 0);
            if ($ulElement !== NULL) {
               $this->getCatalogFromHTML($ulElement, $spiderCategory->id);
            }
        }
    }

    public function updateProducts() {
        $uri = $this->getLinkToListAllProducts();
        $this->browser->go($uri);
        $this->dom = $this->browser->getDOM();
        
        $listProducts = $this->dom->find('div[class="five columns alpha omega item"]');
        foreach($listProducts as $element) {
            $spiderProduct = new SpiderProduct();
            $spiderProduct->category_id = $this->spiderCategory->id;
            
            $spiderProduct->name = trim($element->find('div[class=title] p', 0)->plaintext) . " ";
            $spiderProduct->name .= trim($element->find('p[class=item-title] a', 0)->title);
            
            $spiderProduct->model = trim($element->find('p[class=item-title] a', 0)->title);
            $spiderProduct->model = trim(substr($spiderProduct->model,  stripos($spiderProduct->model , ' ') + 1));
            
                    $spiderProduct->link = $element->find('p[class=item-title] a', 0)->href;
            if (!$spiderProduct->isExist()) {
                $spiderProduct->create();
            }
        }
    }
    
    public function getLinkToListAllProducts() {
        $uri = $this->spiderManufacturer->link . "/" . $this->spiderCategory->link;
        $this->browser->go($uri);
        $this->dom = $this->browser->getDOM();
        
        $linkToAllProducts = $this->dom->find('ul[class=pageNav]', 0);
        if ($linkToAllProducts->last_child() !== null) {
            $uri = $this->spiderManufacturer->link . "/" . $linkToAllProducts->last_child()->find("a", 0)->href;
        }
        
        return $uri;
    }

    public function updateProductDetails() {
        $uri = $this->spiderManufacturer->link . "/" . $this->spiderProduct->link;
        $this->browser->go($uri);
        $this->dom = $this->browser->getDOM();
        
        $detailsProduct = $this->dom->find('div[id=content]', 0);
        $this->spiderProduct->name = trim($detailsProduct->find('h1', 0)->plaintext);
        $this->spiderProduct->name = str_replace("  ", " ", $this->spiderProduct->name);
        
        $uri = $detailsProduct->find('p[class=image-full] img', 0)->src;
        $this->updateImageProduct($uri);

        $description = $detailsProduct->find('div[class=details]', 0)->innertext;
        $description = preg_replace("/(style=(\"|\').+?(\"|\')|class=(\"|\').+?(\"|\'))/", "", $description);
        $this->spiderProduct->description = $description;
        
        $this->spiderProduct->price = $detailsProduct->find('td[class=price] strong', 0)->plaintext;
        $this->spiderProduct->price = str_replace(array(' ', ',', '.'), '', $this->spiderProduct->price);
                
        $this->spiderProduct->done = 1;
        
        $this->spiderProduct->update();
    }
    
    private function updateImageProduct($uri) {
        if (file_exists(ConfigSpider::getPathToImages() . $this->spiderProduct->image)) {
            @unlink(ConfigSpider::getPathToImages() . $this->spiderProduct->image);
        }
        
        $uriToImage = $this->spiderManufacturer->link . $uri;
        $nameImage = time() . "." . $this->getFileExtention($uri);

        $this->browser->download($uriToImage, ConfigSpider::getPathToImages() . $nameImage);
        
        $this->spiderProduct->image = $nameImage;
    }
    
    private function getFileExtention($file) {
        return end(explode(".", $file));
    }
    
}