<?php

class Paginator {
    
    public $countAllItems = 0;
    public $countItemsToPage = 20;
    public $activePage = 0;
    public $countLinks = 10;
    public $uri = '';
    public $text = 'Показывается от {start} до {end} из {total} ({pages} Страниц)';
    public $textFirst = '|&lt;';
    public $textLast = '&gt;|';
    public $textNext = '&gt;';
    public $textPrev = '&lt;';
    public $styleLinks = 'links';
    public $styleResults = 'results';
    public $styleActive = 'active_page';

    public function render() {
        if ($this->activePage < 1) {
            $this->activePage = 1;
        }

        if (!(int)$this->countItemsToPage) {
                $this->countItemsToPage = 10;
        }

        $countPages = ceil($this->countAllItems / $this->countItemsToPage);

        $output = '';

        if ($this->activePage > 1) {
            $output .= " <a href='$this->uri&page=1'>&nbsp;$this->textFirst&nbsp;</a> "
                    . "<a href='$this->uri&page=" . ($this->activePage - 1) . "'>&nbsp;$this->textPrev&nbsp;</a> ";
        }

        if ($countPages > 1) {
            if ($countPages <= $this->countLinks) {
                $start = 1;
                $end = $countPages;
            } else {
                $start = $this->activePage - floor($this->countLinks / 2);
                $end = $this->activePage + floor($this->countLinks / 2);

                if ($start < 1) {
                    $end += abs($start) + 1;
                    $start = 1;
                }

                if ($end > $countPages) {
                    $start -= ($end - $countPages);
                    $end = $countPages;
                }
            }

            if ($start > 1) {
                $output .= ' .... ';
            }

            for ($i = $start; $i <= $end; $i++) {
                if ($this->activePage == $i) {
                    $output .= " <b class='$this->styleActive'> $i </b> ";
                } else {
                    $output .= " <a href='$this->uri&page=$i'>$i</a> ";
                }	
            }

            if ($end < $countPages) {
                $output .= ' .... ';
            }
        }

        if ($this->activePage < $countPages) {
            $output .= " <a href='$this->uri&page=" . ($this->activePage + 1) . "'>&nbsp;$this->textNext&nbsp;</a>"
                    . " <a href='$this->uri&page=$countPages'>&nbsp;$this->textLast&nbsp;</a> ";
        }

        $find = array(
                '{start}',
                '{end}',
                '{total}',
                '{pages}'
        );

        $replace = array(
            ($this->countAllItems) ? (($this->activePage - 1) * $this->countItemsToPage) + 1 : 0,
            ((($this->activePage - 1) * $this->countItemsToPage) > ($this->countAllItems - $this->countItemsToPage)) ? $this->countAllItems : ((($this->activePage - 1) * $this->countItemsToPage) + $this->countItemsToPage),
            $this->countAllItems, 
            $countPages
        );

        return ($output ? '<div class="' . $this->styleLinks . '">' . $output . '</div>' : '') 
            . '<div class="' . $this->styleResults . '">' . str_replace($find, $replace, $this->text) . '</div>';
}
        
}