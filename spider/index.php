<?php
require_once('default.conf.php');
require_once('db.controller.php');

if (isset($_POST['id'])) {
    if ($_POST['name'] != "" && $_POST['link'] != "") {
        $manufacturer = new SpiderManufacturer($_POST['id']);
        $manufacturer->name                  = $_POST['name'];
        $manufacturer->link                  = $_POST['link'];
        $manufacturer->site_manufacturer_id  = $_POST['site_manufacturer_id'];
        $manufacturer->script                = $_POST['script'];
        $manufacturer->done                  = isset($_POST['done']) ? 1 : 0;
                
        if ($_POST['id'] == "") {
            $manufacturer->create();
        } else {
            $manufacturer->update();
        }
        header('Location: .');
    }
}

if (isset($_GET['action'])) {
    $actionManufacturer = new SpiderManufacturer($_GET['id']);
    
    switch ($_GET['action']){
        case "edit":
            break;
        
        case "delete":
            $actionManufacturer->delete();
            header('Location: .');
            break;
        
        case "update_catalog":
            if (!empty($actionManufacturer) && !empty($actionManufacturer->script)) {
                require_once('manufacturer/' . $actionManufacturer->script);
                $manufacturerObject = new ManufacturerClass();
                $manufacturerObject->setSpiderManufacturer($actionManufacturer);
                $manufacturerObject->updateCatalogs();
                $actionManufacturer->update();
                header('Location: .');
            }
            break;
        
        default:
            header('Location: .');
            break;
    }
}

?>
<html>
    <head>
        <style>
           table { 
            border: 4px double black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
           }
           th { 
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           td { 
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           .dark {
                background: #ccc; /* Цвет фона ячеек */
           }
           textarea {
               width: 800px;
               height: 150px;
           }
           input[type=text] {
                width: 800px;
           } 
        </style>          
    </head>
    <body>

        <h2>Добавить\Редактировать производителя</h2>
        <form action="." name="add_edit_manufactorer" method="post">
            <table>
                <tr>
                    <th><b>ID</b></th>
                    <td>
                        <?php echo isset($actionManufacturer) ? $actionManufacturer->id : ""; ?>
                        <input type="hidden" name="id" value="<?php echo isset($actionManufacturer) ? $actionManufacturer->id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Название</b></th>
                    <td>
                        <input type="text" name="name" value="<?php echo isset($actionManufacturer) ? $actionManufacturer->name : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Ссылка</b></th>
                    <td>
                        <input type="text" name="link" value="<?php echo isset($actionManufacturer) ? $actionManufacturer->link : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>ID производителя на сайте</b></th>
                    <td>
                        <input type="text" name="site_manufacturer_id" value="<?php echo isset($actionManufacturer) ? $actionManufacturer->site_manufacturer_id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Скрипт</b></th>
                    <td>
                        <input type="text" name="script" value="<?php echo isset($actionManufacturer) ? $actionManufacturer->script : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Закончен</b></th>
                    <td>
                        <input type="checkbox" name="done" <?php echo (isset($actionManufacturer) && $actionManufacturer->done) ? "checked" : ""; ?> />
                    </td>
                </tr>
                <tr>
                    <th><b>Дата создания</b></th>
                    <td>
                        <?php echo isset($actionManufacturer) ? $actionManufacturer->created_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Дата обновления</b></th>
                    <td>
                        <?php echo isset($actionManufacturer) ? $actionManufacturer->updated_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <button type="submit">Отправить</button>
                        &nbsp;&nbsp;&nbsp;<a href='.'>Отмена</a>
                    </td>
                </tr>
            </table>        
        </form>
        
        
        <h2>Список производителей</h2>
        <table>
            <tr>
                <th><b>#</b></th>
                <th><b>ID</b></th>
                <th><b>Название</b></th>
                <th><b>Ссылка</b></th>
                <th><b>ID производителя на сайте</b></th>
                <th><b>Скрипт</b></th>
                <th><b>Закончен</b></th>
                <th><b>Дата создания</b></th>
                <th><b>Дата обновления</b></th>
                <th><b>Действия</b></th>
            </tr>
<?php             
        $manufacturers = SpiderManufacturer::getManufacturers();
    	foreach ($manufacturers as $key => $manufacturer) {
            
            $outText = "<tr " . (($key % 2) ? "class='dark'" : "") . ">"
                . "<td>" . ($key + 1) . "</td>"
                . "<td>$manufacturer->id</td>";
            
            $countCategories = SpiderCategory::getCountByManufacturer($manufacturer->id);
            if ($countCategories > 0) {
                $outText .= "<td><a href=\"./categories.php?manufacturer_id=$manufacturer->id\">$manufacturer->name</a></td>";
                $countCategoriesIsDone = SpiderCategory::getCountByManufacturerIsDone($manufacturer->id);
                if ($countCategories == $countCategoriesIsDone && $manufacturer->done == 0) {
                    $manufacturer->done = 1;
                    $manufacturer->update();
                } else if ($countCategories != $countCategoriesIsDone && $manufacturer->done == 1) {
                    $manufacturer->done = 0;
                    $manufacturer->update();
                }
            } else{
                $outText .= "<td>$manufacturer->name</td>";
            }
            $outText .= "<td><a href='$manufacturer->link' target='_blank'>$manufacturer->link</a></td>"
                . "<td>$manufacturer->site_manufacturer_id</td>"
                . "<td>$manufacturer->script</td>"
                . "<td><b style='color:" . ($manufacturer->done ? "green'>Да" : "red'>Нет") . "</b></td>"
                . "<td>$manufacturer->created_at</td>"
                . "<td>$manufacturer->updated_at</td>"
                . "<td>"
                    . "<a href='.?action=delete&id=$manufacturer->id' onclick=\"return confirm('Вы уверены, что хотите удалить - $manufacturer->name?')\">Удалить</a>"
                    . "&nbsp;&nbsp;&nbsp;&nbsp;<a href='.?action=edit&id=$manufacturer->id'>Редактировать</a>"
                    . "&nbsp;&nbsp;&nbsp;&nbsp;<a href='.?action=update_catalog&id=$manufacturer->id' onclick=\"return confirm('Вы уверены, что хотите обновить - $manufacturer->name?')\">Обновить каталоги</a>"
                . "</td>"
            . "</tr>";
            
            echo $outText;
        }
?>
        </table>
    </body>    
</html>