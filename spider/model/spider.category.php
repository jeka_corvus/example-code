<?php
require_once('model.php');

class SpiderCategory extends Model {
    private static $tableName = "spider_category";
    
    public $id = 0;
    public $parent_id;
    public $manufacturer_id;
    public $name;
    public $link;
    public $site_parent_category_id = 0;
    public $site_category_id = 0;
    public $done = 0;
    public $created_at;
    public $updated_at;
    
    public function __construct($id = 0) {
        parent::__construct();
        
        $this->listRows = array(
        'id',
        'parent_id',
        'manufacturer_id',
        'name', 
        'link',
        'site_parent_category_id',
        'site_category_id',
        'done',
        'created_at',
        'updated_at');
        
        if ($id != 0 && $id != "" && !empty($id)) {
            $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `id`=" . $id;
            $category = DBController::getInstance()->query($sql);
            if (count($category->row) > 0) {
                $this->mappingFromArray($category->row);
            }
        }
    }
    
    public static function getCategories($manufacturerId, $parentId) {
        $sql = "SELECT * FROM `" . self::$tableName . "`"
               . " WHERE `manufacturer_id`='$manufacturerId' AND `parent_id`='$parentId'"
               . " ORDER BY `updated_at` ASC, `id` ASC";
        $result = DBController::getInstance()->query($sql);
        
        $listCategories = array();
        foreach ($result->rows as $category) {
            $spiderCategory = new SpiderCategory();
            $spiderCategory->mappingFromArray($category);
            $listCategories[] = $spiderCategory;
        }
        
        return $listCategories;
    }
    
    public static function getCategoriesTreeByManufacturer($manufacturerId) {
        $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `manufacturer_id`='$manufacturerId' ORDER BY `id`";
        $result = DBController::getInstance()->query($sql);
        
        $listCategories = array();
        $mapElements = array();
        $mapElements[0] = &$listCategories;
        foreach ($result->rows as $category) {
            $spiderCategory = new SpiderCategory();
            $spiderCategory->mappingFromArray($category);
            
            $element = &$mapElements[$spiderCategory->parent_id];
            $element[$spiderCategory->id] = array();
            $element[$spiderCategory->id][0] = $spiderCategory;
                    
            $mapElements[$spiderCategory->id] = &$element[$spiderCategory->id][1];
        }
        
        return $listCategories;
    }

    public static function getCountByParent($parentId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` WHERE `parent_id`='$parentId'";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public static function getCountByManufacturerAndParent($manufacturerId, $parentId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` WHERE `parent_id`='$parentId'"
                . " AND `manufacturer_id`=$manufacturerId";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public static function getCountByParentIsDone($parentId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` WHERE `parent_id`='$parentId'"
                . " AND `done`=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public static function getCountByManufacturerAndParentIsDone($manufacturerId, $parentId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` WHERE `parent_id`='$parentId'"
                . " AND `done`=1 AND `manufacturer_id`=$manufacturerId";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public static function getCountByManufacturerIsDone($manufacturerId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName 
                . "` WHERE `manufacturer_id`='$manufacturerId' AND `done`=1";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public static function getCountByManufacturer($manufacturerId) {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "` WHERE `manufacturer_id`='$manufacturerId'";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }

    public function create() {
        $sql = "INSERT INTO `" . self::$tableName . "` SET ";

        $dateNow = $this->getDateNow();
        $this->created_at = $dateNow;
        $this->updated_at = $dateNow;
        
        $sql = $this->mappingSQL($sql);
        
        DBController::getInstance()->query($sql);

        $this->id = DBController::getInstance()->getLastId();
    }

    public function isExist() {
        $sql = "SELECT * FROM `" . self::$tableName . "` WHERE"
                . " parent_id='$this->parent_id' AND  manufacturer_id='$this->manufacturer_id'"
                . " AND link='$this->link'";
        
        $result = DBController::getInstance()->query($sql);

        if (count($result->row) > 0) {
            $this->mappingFromArray($result->row);
        }
        
        return $this->id > 0 ? true : false;
    }

    public function update() {
        $sql = "UPDATE `" . self::$tableName . "` SET ";

        $this->updated_at = $this->getDateNow();
        $sql = $this->mappingSQL($sql);

        $sql .= " WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public function deleteWithObjects() {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
        $listCategories = self::getCategoriesByManufacturer($this->manufacturer_id, $this->id);
        foreach ($listCategories as $category) {
            $category->delete();
        }
    }
    
    public function delete() {
        $listIdCategories = array($this->id);
        $spiderProduct = new SpiderProduct();
        do {
            $spiderProduct->deleteByListCategories($listIdCategories);
            $sql = "DELETE FROM `" . self::$tableName . "` WHERE `id` IN (" . join(',', $listIdCategories) . ")";
            DBController::getInstance()->query($sql);

            $sql = "SELECT `id` FROM `" . self::$tableName . "` WHERE `parent_id` IN (" . join(',', $listIdCategories) . ")";
            $listCategories = DBController::getInstance()->query($sql);
            
            $listIdCategories = array();
            foreach ($listCategories->rows as $categoryId) {
                $listIdCategories[] = $categoryId['id'];
            }
        } while(count($listIdCategories) > 0);
    }
    
    public static function deleteByManufacturer($manufacturerId) {
        $listCategories = self::getCategories($manufacturerId, 0);
        foreach ($listCategories as $category) {
            $category->delete();
        }
    }
 
    public function synchronizationWithSite() {
        if ($this->done != 1) {
            return;
        }
        
        if ($this->site_category_id == 0) {
            $this->createInSite();
        } else {
            $this->updateInSite();
        }

        SpiderProduct::synchronizationWithSiteByCategory($this->id);
        
        $listCategories = self::getCategories($this->manufacturer_id, $this->id);
        foreach ($listCategories as $category) {
            $category->synchronizationWithSite();
        }
    }
    
    private function createInSite() {
        $dateNow = $this->getDateNow();

        $slq = "INSERT INTO `oc_category` SET"
                . " `parent_id`=$this->site_parent_category_id"
                . ", `image`=''"
                . ", `top`=0"
                . ", `column`=1"
                . ", `sort_order`=0"
                . ", `status`=0"
                . ", `date_added`='$dateNow'"
                . ", `date_modified`='$dateNow'";
        DBController::getInstance()->query($slq);
        $this->site_category_id = DBController::getInstance()->getLastId();
        $this->update();
        
        $slq = "INSERT INTO `oc_category_description` SET"
                . " `category_id`=$this->site_category_id"
                . ", `language_id`=1"
                . ", `name`='$this->name'"
                . ", `description`=''"
                . ", `meta_description`=''"
                . ", `description_bottom`=''"
                . ", `meta_keyword`=''"
                . ", `seo_title`='$this->name'"
                . ", `seo_h1`='$this->name'";
        DBController::getInstance()->query($slq);
        
        $slq = "INSERT INTO `oc_category_to_store` SET"
                . " `category_id`=$this->site_category_id"
                . ", `store_id`=0";
        DBController::getInstance()->query($slq);
        
        $this->insertToCategoryPath($this->site_category_id, $level = 0);
        
        $slq = "INSERT INTO `oc_url_alias` SET"
                . " `query`='category_id=$this->site_category_id'"
                . ", `keyword`='" . Utility::encodeLowerStringToTranslitTrim($this->name) . "'"
                . ", `seomanager`=0";
        DBController::getInstance()->query($slq);
        
        $sql = "UPDATE `" . self::$tableName . "` SET `site_parent_category_id`=$this->site_category_id"
                . " WHERE `parent_id`=$this->id";
        DBController::getInstance()->query($sql);
    }
    
    private function insertToCategoryPath($parentId, &$level) {
        $sql = "SELECT `parent_id` FROM `oc_category` WHERE `category_id`=$parentId";
        $result = DBController::getInstance()->query($sql);
        
        if ($result->row['parent_id'] != 0) {
            $this->insertToCategoryPath($result->row['parent_id'], $level);
        }
        
        $slq = "INSERT INTO `oc_category_path` SET"
                . " `category_id`=$this->site_category_id"
                . ", `path_id`=$parentId"
                . ", `level`=$level";
        DBController::getInstance()->query($slq);
        ++$level;
    }
    
    private function updateInSite() {
        $dateNow = $this->getDateNow();

        $slq = "UPDATE `oc_category` SET"
                . " `date_modified`='$dateNow'"
                . " WHERE `category_id`=$this->site_category_id";
        DBController::getInstance()->query($slq);
        
        $slq = "UPDATE `oc_category_description` SET"
                . " `name`='$this->name'"
                . ", `seo_title`='$this->name'"
                . ", `seo_h1`='$this->name'"
                . " WHERE `category_id`=$this->site_category_id"
                . " AND `language_id`=1";
        DBController::getInstance()->query($slq);
        
        $slq = "UPDATE `oc_url_alias` SET"
                . " `keyword`='" . Utility::encodeLowerStringToTranslitTrim($this->name) . "'"
                . " WHERE `query`='category_id=$this->site_category_id'";
        DBController::getInstance()->query($slq);
    }
    
    public function setEmptySiteIdAndParent() {
        $listIdCategories = array($this->id);
        $spiderProduct = new SpiderProduct();
        do {
            $spiderProduct->setEmptySiteId($listIdCategories);
            $sql = "UPDATE `" . self::$tableName . "` SET"
                    . " `site_parent_category_id`=0, site_category_id=0"
                    . " WHERE `id` IN (" . join(',', $listIdCategories) . ")";
            DBController::getInstance()->query($sql);

            $sql = "SELECT `id` FROM `" . self::$tableName . "` WHERE `parent_id` IN (" . join(',', $listIdCategories) . ")";
            $listCategories = DBController::getInstance()->query($sql);
            
            $listIdCategories = array();
            foreach ($listCategories->rows as $categoryId) {
                $listIdCategories[] = $categoryId['id'];
            }
        } while(count($listIdCategories) > 0);
    }
}