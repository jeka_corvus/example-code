﻿function reloadPage(waitSeconds) {
    var reload = true;
	var ret;
	var isError = false;

	while (reload) {
		isError = false;
		ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"Request Timeout\" IGNORE_CASE=YES");		
		if (ret > 0) {
			isError = true;
		} else {
			ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"Your ip is blocked\" IGNORE_CASE=YES");		
		}
		if (!isError && ret > 0) {
			isError = true;
		} else {
			ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"Fatal error\" IGNORE_CASE=YES");		
		}
		if (!isError && ret > 0) {
			isError = true;
		} else {
			ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"Попытка соединения не удалась\" IGNORE_CASE=YES");		
		}
		if (!isError && ret > 0) {
			isError = true;
		} else {
			ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"<title>MyStart</title>\" IGNORE_CASE=YES");	
		}
		if (!isError && ret > 0) {
			// EDIT resource_id=1
			ret = iimPlay("CODE:URL GOTO=http://fenixtorg.ru/admin/mailer/contact.php?resource_id=27");		
			isError = true;
		}

		if (isError) {
			iimPlay("CODE:ADD !EXTRACT {{!URLCURRENT}}");
			var uri = iimGetLastExtract();

			var resourceId = /resource_id=([^&]+)/.exec(uri)[1];
			var oldNumSite = /num_site=([^&]+)/.exec(uri);
			var numSite = oldNumSite ? (parseInt(oldNumSite[1]) + 1).toString() : '0';
			ret = iimPlay("CODE:URL GOTO=http://fenixtorg.ru/admin/mailer/contact.php?resource_id=" + resourceId + "&auto_action=update_next_organization_emails&num_site=" + numSite + "&num_block=0");
		}
		
		ret = iimPlay("CODE:WAIT SECONDS=" + waitSeconds)
	}
}

reloadPage(10)