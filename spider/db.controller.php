<?php
require_once('mysql.php');
// Configuration
if (file_exists('../config.php')) {
	require_once('../config.php');
}
require_once('model/spider.manufacturer.php');
require_once('model/spider.category.php');
require_once('model/spider.product.php');

class DBController {
    
    private $db;
    
    private static $instance = null;
    
    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    private function __clone() {}
    private function __construct() {
        $this->db = new MySQL(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);        
    }

    public function query($sql) {
        return $this->db->query($sql);
    }
    
    public function getLastId() {
        return $this->db->getLastId();
    }
    
    public function escape($str) {
        return $this->db->escape($str);
    }
    
}
