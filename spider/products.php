<?php
require_once('default.conf.php');
require_once('db.controller.php');

if (isset($_GET['category_id'])) {
    $categoryGeneral = new SpiderCategory($_GET['category_id']);
    $manufacturerGeneral = new SpiderManufacturer($categoryGeneral->manufacturer_id);
} else {
    echo "В адресной строке нет ID категории!!!";
    die;
}

if (isset($_POST['id'])) {
    if ($_POST['name'] != "" && $_POST['link'] != "") {
        $spiderProduct = new SpiderProduct($_POST['id']);
        $spiderProduct->name                = $_POST['name'];
        $spiderProduct->model               = $_POST['model'];
        $spiderProduct->link                = $_POST['link'];
        $spiderProduct->site_product_id     = $_POST['site_product_id'];
        $spiderProduct->description         = $_POST['description'];
        $spiderProduct->price               = $_POST['price'];
        $spiderProduct->meta_description    = $_POST['meta_description'];
        $spiderProduct->done                = isset($_POST['done']) ? 1 : 0;
                
        $spiderProduct->update();
        
        header("Location: ./products.php?category_id=$categoryGeneral->id");
    }
}

if (isset($_GET['action'])) {
    if (isset($_GET['id'])) {
        $actionProduct = new SpiderProduct($_GET['id']);
    } else {
        $actionProduct = new SpiderProduct();
        $actionProduct->getNextProductsNotDone($categoryGeneral->id);
    }
    
    switch ($_GET['action']){
        case "edit":
            break;
        
        case "delete":
            $actionProduct->delete();
            header("Location: ./products.php?category_id=$categoryGeneral->id");
            break;
        
        case "update_product":
            if (!empty($manufacturerGeneral) && !empty($manufacturerGeneral->script)) {
                require_once('manufacturer/' . $manufacturerGeneral->script);
                $manufacturerObject = new ManufacturerClass();
                $manufacturerObject->setSpiderManufacturer($manufacturerGeneral);
                $manufacturerObject->setSpiderCategory($categoryGeneral);
                $manufacturerObject->setSpiderProduct($actionProduct);
                $manufacturerObject->updateProductDetails();
            }
            header("Location: ./products.php?category_id=$categoryGeneral->id");
            break;

        case "update_next_product":
            if (!empty($manufacturerGeneral) && !empty($manufacturerGeneral->script) && ($actionProduct->id != 0)) {
                require_once('manufacturer/' . $manufacturerGeneral->script);
                $manufacturerObject = new ManufacturerClass();
                $manufacturerObject->setSpiderManufacturer($manufacturerGeneral);
                $manufacturerObject->setSpiderCategory($categoryGeneral);
                $manufacturerObject->setSpiderProduct($actionProduct);
                $manufacturerObject->updateProductDetails();
                header("Location: ./products.php?category_id=$categoryGeneral->id&auto_action=update_next_product");
            } else {
                header("Location: ./products.php?category_id=$categoryGeneral->id");
            }
            break;

        case "synchronization":
            if ($categoryGeneral->site_parent_category_id == 0) {
                echo "<script type='text/javascript'>alert('Не заполнен \'ID родительской категории на сайте\'!!!');</script>";
                unset($actionProduct);
                header("Location: ./products.php?category_id=$categoryGeneral->id");
            } else {
                $actionProduct->synchronizationWithSite();
            }
            break;
        
        default:
            header("Location: ./products.php?category_id=$categoryGeneral->id");
            break;
    }
}

?>
<html>
    <head>
        <script type="text/javascript" src="./js/products.js"></script>
        <?php if (isset($_GET['auto_action']) && $_GET['auto_action'] == 'update_next_product') { ?>
            <script type="text/javascript">startUpdateProducts();</script>
        <?php } ?>
        <style>
           table { 
            border: 4px double black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
           }
           th { 
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           td { 
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           .dark {
                background: #ccc; /* Цвет фона ячеек */
           }
           textarea {
               width: 800px;
               height: 150px;
           }
           input[type=text] {
                width: 800px;
           } 
        </style>          
    </head>
    <body>

    <?php if (isset($actionProduct)) { ?>
        <h2>Редактировать товар</h2>
        <form action="<?php echo "./products.php?category_id=$categoryGeneral->id&id=$actionProduct->id"?>" name="edit_product" method="post">
            <table>
                <tr>
                    <th><b>ID</b></th>
                    <td>
                        <?php echo isset($actionProduct) ? $actionProduct->id : ""; ?>
                        <input type="hidden" name="id" value="<?php echo isset($actionProduct) ? $actionProduct->id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Категория ID</b></th>
                    <td>
                        <?php echo isset($actionProduct) ? $actionProduct->category_id : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Название</b></th>
                    <td>
                        <input type="text" name="name" value="<?php echo isset($actionProduct) ? $actionProduct->name : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Модель</b></th>
                    <td>
                        <input type="text" name="model" value="<?php echo isset($actionProduct) ? $actionProduct->model : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Ссылка</b></th>
                    <td>
                        <input type="text" name="link" value="<?php echo isset($actionProduct) ? $actionProduct->link : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>ID товара на сайте</b></th>
                    <td>
                        <input type="text" name="site_product_id" value="<?php echo isset($actionProduct) ? $actionProduct->site_product_id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Картинка</b></th>
                    <td>
                        <img  width='100' height='100'  src="./<?php echo isset($actionProduct) ? ConfigSpider::$folderToImages . $actionProduct->image : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Описание</b></th>
                    <td>
                        <textarea name="description"><?php echo isset($actionProduct) ? $actionProduct->description : ""; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th><b>Цена</b></th>
                    <td>
                        <input type="text" name="price" value="<?php echo isset($actionProduct) ? $actionProduct->price : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>META Description</b></th>
                    <td>
                        <textarea name="meta_description"><?php echo isset($actionProduct) ? $actionProduct->meta_description : ""; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th><b>Закончен</b></th>
                    <td>
                        <input type="checkbox" name="done" <?php echo (isset($actionProduct) && $actionProduct->done) ? "checked" : ""; ?> />
                    </td>
                </tr>
                <tr>
                    <th><b>Дата создания</b></th>
                    <td>
                        <?php echo isset($actionProduct) ? $actionProduct->created_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Дата обновления</b></th>
                    <td>
                        <?php echo isset($actionProduct) ? $actionProduct->updated_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <button type="submit">Отправить</button>
                        </form>
                        &nbsp;&nbsp;&nbsp;<a href="<?php echo "./products.php?category_id=$categoryGeneral->id" ?>">Отмена</a>
                    </td>
                </tr>
            </table>        
    <?php
    }
    ?>
            <a id='href_to_update' href="<?php echo "./products.php?category_id=$categoryGeneral->id&action=update_next_product" ?>">
                <h1>Обновить товары<b id='time_to_update'></b></h1>
            </a>
            <a href='<?php echo "./products.php?category_id=$categoryGeneral->id" ?>'>Отмена авто обновления</a>
            
        <?php 
            $backLink = "";
            $idCategory = $_GET['category_id'];
            do {
                $categoryBack = new SpiderCategory($idCategory);
                $backLink = "-><a href='categories.php?manufacturer_id=$manufacturerGeneral->id&id=$categoryBack->parent_id'>"
                    . "$categoryBack->name</a>" . $backLink;
                $idCategory = $categoryBack->parent_id;
            } while ($idCategory != 0);
        ?>
    
        <h2>Статистика</h2>
        <table>
            <tr>
                <th><b>Всех товаров</b></th>
                <th><b>Готовых товаров</b></th>
                <th><b>Незаконченых товаров</b></th>
            </tr>
            <tr>
                <td><b><?php echo $countProductsAll = SpiderProduct::getCountByCategory($categoryGeneral->id) ?></b></td>
                <td style='color: green'><b><?php echo $countProductsIsDone = SpiderProduct::getCountByCategoryIsDone($categoryGeneral->id) ?></b></td>
                <td style='color: red'><b><?php echo $countProductsAll - $countProductsIsDone ?></b></td>
            </tr>
        </table>
            
        <br/>
        <h2><a href="./"><?php echo $manufacturerGeneral->name; ?></a><?php echo $backLink ?></a></h2>
        <h2>Список товаров</h2>
        <table>
            <tr>
                <th><b>#</b></th>
                <th><b>ID</b></th>
                <th><b>Категория ID</b></th>
                <th><b>Название</b></th>
                <th><b>Модель</b></th>
                <th><b>Ссылка</b></th>
                <th><b>ID товара на сайте</b></th>
                <th><b>Картинка</b></th>
                <th><b>Описание</b></th>
                <th><b>Цена</b></th>
                <th><b>META Description</b></th>
                <th><b>Закончен</b></th>
                <th><b>Дата создания</b></th>
                <th><b>Дата обновления</b></th>
                <th><b>Действия</b></th>
            </tr>
<?php             
        $products = SpiderProduct::getProductsByCategory($categoryGeneral->id);
        
        foreach ($products as $key => $product) {
            $outText = "<tr " . (($key % 2) ? "class='dark'" : "") . ">"
                . "<td>" . ($key + 1) . "</td>"
                . "<td>$product->id</td>"
                . "<td>$product->category_id</td>"
                . "<td>$product->name</td>"
                . "<td>$product->model</td>"
                . "<td><a href='$manufacturerGeneral->link/$product->link' target='_blank'>Ссылка на сайт производителя</a></td>"
                . "<td>$product->site_product_id</td>";
            
            if (empty($product->image)) {
                $outText .= "<td>$product->image</td>";
            } else {
                $outText .= "<td><img  width='100' height='100'  src=\"./" . ConfigSpider::$folderToImages . $product->image . "\" /></td>";
            }
                
            $outText .= "<td>$product->description</td>"
                . "<td>$product->price</td>"
                . "<td>$product->meta_description</td>"
                . "<td><b style='color:" . ($product->done ? "green'>Да" : "red'>Нет") . "</b></td>"
                . "<td>$product->created_at</td>"
                . "<td>$product->updated_at</td>"
                . "<td><table style='border: 0px;'>"
                    . "<tr><td><a href='./products.php?category_id=$categoryGeneral->id&id=$product->id&action=delete' onclick=\"return confirm('Вы уверены, что хотите удалить - \'$product->name\'?')\">Удалить</a><br /><br /></td></tr>"
                    . "<tr><td><a href='./products.php?category_id=$categoryGeneral->id&id=$product->id&action=edit'>Редактировать</a><br /><br /></td></tr>"
                    . "<tr><td><a href='./products.php?category_id=$categoryGeneral->id&id=$product->id&action=update_product' onclick=\"return confirm('Вы уверены, что хотите обновить - $product->name?'\">Обновить товар</a><br /><br /></td></tr>"
                    . "<tr><td><a href='./products.php?category_id=$categoryGeneral->id&id=$product->id&action=synchronization' onclick=\"return confirm('Вы уверены, что хотите синхронизировать с сайтом - \'$product->name\'?')\">Синхронизация</a></td></tr>"
                . "</table></td>"
            . "</tr>";
            echo $outText;
        }
?>
        </table>
    </body>    
</html>