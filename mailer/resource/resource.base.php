<?php
require_once('browser.php');

class ResourceBase {
    protected $browser;
    protected $dom;
    protected $mailerResource;
    protected $mailerOrganization;
    protected $mailerContact;
    
    private $listContactPages;
    private $listExcludeEmails;
    private $countPagesInBlock = 5;

    public function __construct() { 
        $this->browser = new Browser();
        
        $this->listContactPages = array(
            '',
            '/contact', '/contact/', '/contact.html', '/contact.htm', '/contact.xml', '/contact.php',
            '/contacts', '/contacts/', '/contacts.html', '/contacts.htm', '/contacts.xml', '/contacts.php',
            '/kontakty', '/kontakty/', '/kontakty.html', '/kontakty.htm', '/kontakty.xml', '/ru/kontakty',
            '/about/contacts', '/about/contacts/', '/about/contacts.php', '/about/contacts.html', '/about/contacts.htm',
            '/info/kontakty.aspx',
            '/site/contact', '/site/contact/',
            '/ru/contacts', '/ru/contacts/', '/ru/kontakty', '/ru/kontakty/', '/ru/contact/', '/ru/contact',
            '/company/contacts', '/company/contacts/',
            '/our-location', '/our-location/',
            '/customer-service/', '/customer-service',
            '/kontaktnaya_informaciya/', '/kontaktnaya_informaciya',
            '/address/', '/address',
            '/contacts/cooperation', '/?page=contacts', '/o-kompanii/contact/', '/contacts-details.html',
            '/shop/feedback/');
        
        $this->listExcludeEmails = array(
          'Rating@Mail.ru'  
        );
    }

    public function setMailerResource($mailerResource) {
        $this->mailerResource = $mailerResource;
    }

    public function setMailerOrganization($mailerOrganization) {
        $this->mailerOrganization = $mailerOrganization;
    }

    public function setMailerContact($mailerContact) {
        $this->mailerContact = $mailerContact;
    }
    
    public function updateEmails($numberSite = 0, $numberBlockContactPages = 0) {
        $listSites = explode(';', $this->mailerContact->site);
        $countAllSites = count($listSites);
        
        if (!isset($listSites[$numberSite]) || empty($listSites[$numberSite])) {
            if ($numberSite >= $countAllSites - 1) {
                $this->mailerContact->emails_done = 1;
                $this->mailerContact->update();
                return false;
            } else {
                return array(++$numberSite, 0);
            }
        }

        $site = $listSites[$numberSite];
//        $site = "http://www.darbazirest.ru/contacts/";

        $uri = Utility::encodeUri($site);
        $siteNew = $this->browser->getUrl($uri);

        if (empty($siteNew[0])) {
            if ($numberSite <= $countAllSites - 1) {
                $this->mailerContact->emails_done = 1;
                $this->mailerContact->update();
                return false;
            } else {
                return array(++$numberSite, 0);
            }
        }

        $site = $siteNew[1]['url'];
        if (strripos($site, '/') == strlen($site) - 1) {
            $site = substr($site, 0, strlen($site) - 1);
        }

        $indexPage = $numberBlockContactPages * $this->countPagesInBlock;
        $stopIndex = $indexPage + $this->countPagesInBlock;
        $countAllPages = count($this->listContactPages);
        $searchEmails = false;
        for ($indexPage; $indexPage < $stopIndex && $indexPage < $countAllPages; $indexPage++) {
//            // DELETE
//            $this->listContactPages[$indexPage] = "/kontakty"; 
            
            $uri = rawurlencode($site . $this->listContactPages[$indexPage]);
            $uri = str_replace(array('%2F', '%3A'), array('/', ':'), $uri);
            $content = $this->browser->go($uri);
            
            if (stripos($content, 'Your ip is blocked') !== false) {
                break;
            }

            $pattern = '/\b([a-z0-9._-]+@[a-z0-9.-]+\.[a-z0-9.-]+)\b/i';
//            $pattern = '/\b([a-z0-9._-]+@[a-z0-9.-]+\.[^(?:jp(?:e?g|e|2)|gif|png|tiff?|bmp|ico)$][a-z0-9.-]+)\b/i';
            preg_match_all($pattern, $content, $emails);

            $listEmails = array();
            if (trim($this->mailerContact->email) != "") {
                $listEmails = explode('|', trim($this->mailerContact->email));
                unset($listEmails[count($listEmails) - 1]);
                unset($listEmails[0]);
            }

            $countNewEmails = 0;
            if (isset($emails[0]) && count($emails[0]) > 0) {
                foreach ($emails[0] as $email) {
                    $cleanEmail = trim($email);
                    if (!in_array($cleanEmail, $this->listExcludeEmails) && !in_array($cleanEmail, $listEmails)
                            && !$this->mailerContact->isExistEmail($cleanEmail)) {
                        $listEmails[] = $cleanEmail;
                        ++$countNewEmails;
                    }
                }
            }

            if (isset($emails[1]) && count($emails[1]) > 0) {
                foreach ($emails[1] as $email) {
                    $cleanEmail = trim($email);
                    if (!in_array($cleanEmail, $this->listExcludeEmails) && !in_array($cleanEmail, $listEmails)
                            && !$this->mailerContact->isExistEmail($cleanEmail)) {
                        $listEmails[] = $cleanEmail;
                        ++$countNewEmails;
                    }
                }
            }

            echo "<pre>";
            print_r($site . $this->listContactPages[$indexPage]);
            echo "<br />";
            print_r($uri);
            echo "<br />";
            print_r($emails);
            echo "<br />";
            print_r($listEmails);
//            echo "<br />";
//            print_r($content);
            echo "</pre>" . PHP_EOL;
//                die;
//            if ($this->listContactPages[$indexPage] == '/contacts') {
//                die;
//            }

            if ($countNewEmails > 0) {
                $uniqueListEmails = array_unique($listEmails);
                $this->mailerContact->email = "|" . join('|', $uniqueListEmails) . "|";
                $this->mailerContact->clearEmails();
                $this->mailerContact->update();
                if ($indexPage > 0) {
                    $searchEmails = true;
                    break;
                }
            }
        }
//        }
        // если это последний сайт, то оправляет пустую, т.е. к следующему контакту
        if ($numberSite == $countAllSites - 1 && ($indexPage >= $countAllPages - 1 || $searchEmails)) {
            $this->mailerContact->emails_done = 1;
            $this->mailerContact->update();
            return true;
        }
        
        // если сайты еще не закончились, но emails нашлись или не нашлись, но блоки 
        // страниц закончились, то оправляем на следующий сайт
        if ($searchEmails || $indexPage >= $countAllPages - 1) {
            return array(++$numberSite, 0);
        }
            
        return array($numberSite, ++$numberBlockContactPages);
   }

}
