﻿function reloadPage(waitSeconds) {
    var reload = true;
	var ret;
	var isError = false;

	while (reload) {
		isError = false;
		ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"Request Timeout\" IGNORE_CASE=YES");		
		if (ret > 0) {
			isError = true;
		} else {
			ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"Your ip is blocked\" IGNORE_CASE=YES");		
		}
		if (!isError && ret > 0) {
			isError = true;
		} else {
			ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"Попытка соединения не удалась\" IGNORE_CASE=YES");		
		}
		if (!isError && ret > 0) {
			isError = true;
		} else {
			ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"Fatal error\" IGNORE_CASE=YES");		
		}
		if (!isError && ret > 0) {
			isError = true;
		} else {
			ret = iimPlay("CODE:SEARCH SOURCE=TXT:\"<title>MyStart</title>\" IGNORE_CASE=YES");	
		}
		if (!isError && ret > 0) {
			isError = true;
		}

		if (isError) {
			ret = iimPlay("CODE:URL GOTO=http://fenixtorg.ru/admin/mailer/organization.php?resource_id=27&action=update_next_organization");
		}
		
		ret = iimPlay("CODE:WAIT SECONDS=" + waitSeconds)
	}
}

reloadPage(10)