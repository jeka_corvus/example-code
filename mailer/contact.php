<?php
require_once('default.conf.php');
require_once('db.controller.php');
require_once('utility/paginator.php');


//MailerContact::clearAllContactEmails($_GET['resource_id']);
//MailerContact::deleteContactWithEmptyEmails($_GET['resource_id']);


if (isset($_GET['resource_id'])) {
    $resourceGeneral = new MailerResource($_GET['resource_id']);
} else {
    echo "В адресной строке нет ID ресурса!!!";
    die;
}

$pageParams = "";
if (isset($_GET['page'])) {
    $pageParams = '&page=' . $_GET['page'];
}

if (isset($_POST['id'])) {
    if ($_POST['title'] != "") {
        $mailerContact = new MailerContact($_POST['id']);
        $mailerContact->name                = $_POST['name'];
        $mailerContact->title               = $_POST['title'];
        $mailerContact->phone               = $_POST['phone'];
        $mailerContact->site                = $_POST['site'];
        $mailerContact->emails_done         = isset($_POST['emails_done']) ? 1 : 0;
        $mailerContact->organization_id     = $_POST['organization_id'];
        $mailerContact->done                = isset($_POST['done']) ? 1 : 0;
        $mailerContact->email               = "";
        
        if (trim($_POST['email']) != "") {
            $listEmails = explode('|', trim($_POST['email']));
            unset($listEmails[count($listEmails) - 1]);
            unset($listEmails[0]);
            $uniqueListEmails = array_unique($listEmails);
            foreach($uniqueListEmails as $email) { 
                if ($mailerContact->isExistEmail($email)) {
                    continue;
                }
                $mailerContact->email .= ($mailerContact->email == "") ? "|" : "";
                $mailerContact->email .= $email . "|";
            }
        }
        
        if ($_POST['id'] == "") {
            $mailerContact->create();
        } else {
            $mailerContact->update();
        }
        
        header("Location: ./contact.php?resource_id=$resourceGeneral->id" . $pageParams);
    }
}


if (isset($_GET['action'])) {
    if (isset($_GET['id'])) {
        $actionContact = new MailerContact($_GET['id']);
    }
    
    switch ($_GET['action']){
        case "edit":
            break;
        
        case "delete":
            $actionContact->delete();
            header("Location: ./contact.php?resource_id=$resourceGeneral->id" . $pageParams);
            break;
        
        case "update_next_organization_contacts":
            $actionOrganization = new MailerOrganization();
            $actionOrganization->getNextOrganizationNotDone($resourceGeneral->id);
            
            if (!empty($resourceGeneral) && ($actionOrganization->uri != "")) {
                require_once('resource/' . $resourceGeneral->script);
                $resourceObject = new ResourceClass();
                $resourceObject->setMailerResource($resourceGeneral);
                $resourceObject->setMailerOrganization($actionOrganization);
                $resourceObject->updateContacts();
                header("Location: ./contact.php?resource_id=$resourceGeneral->id&auto_action=update_next_organization_contacts" . $pageParams);
            } else {
                header("Location: ./contact.php?resource_id=$resourceGeneral->id" . $pageParams);
            }
            break;
        
        case "update_next_organization_emails":
            $mailerContact = MailerContact::getNextContactNotEmailsDone($resourceGeneral->id);
            
            if (!empty($resourceGeneral) && (!is_null($mailerContact))) {
                require_once('resource/' . $resourceGeneral->script);
                $resourceObject = new ResourceClass();
                $resourceObject->setMailerResource($resourceGeneral);
                $resourceObject->setMailerContact($mailerContact);
                $result = $resourceObject->updateEmails(isset($_GET['num_site']) ? $_GET['num_site'] : 0,
                        isset($_GET['num_block']) ? $_GET['num_block'] : 0);
                
                if (is_array($result)) {
                    header("Location: ./contact.php?resource_id=$resourceGeneral->id&auto_action=update_next_organization_emails&num_site=" . $result[0] . "&num_block=" . $result[1]. $pageParams);
                    break;
                }
                header("Location: ./contact.php?resource_id=$resourceGeneral->id&auto_action=update_next_organization_emails" . $pageParams);
            } else {
                header("Location: ./contact.php?resource_id=$resourceGeneral->id" . $pageParams);
            }
            break;

        default:
            header("Location: ./contact.php?resource_id=$resourceGeneral->id" . $pageParams);
            break;
    }
}

?>
<html>
    <head>
        <title>Mailer Contact</title>
        <script type="text/javascript" src="./js/utility.js"></script>
        <?php if (isset($_GET['auto_action']) && $_GET['auto_action'] == 'update_next_organization_contacts') { ?>
            <script type="text/javascript">startNextUpdate();</script>
        <?php } ?>
        <?php if (isset($_GET['auto_action']) && $_GET['auto_action'] == 'update_next_organization_emails') { ?>
            <script type="text/javascript">startNextUpdateWithParams('href_to_update_email', 'time_to_update_email', 4);</script>
        <?php } ?>
        <style>
           table { 
            border: 4px double black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
           }
           th { 
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           td { 
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           .dark {
                background: #ccc; /* Цвет фона ячеек */
           }
           textarea {
               width: 800px;
               height: 150px;
           }
           input[type=text] {
                width: 800px;
           }
           .active_page {
               font-size: 25px;
           }
        </style>          
    </head>
    <body>

    <h2><a href="#" onclick="showHideElement('block_add_edit'); return false;">Добавить/Редактировать контакт</a></h2>
    <div id="block_add_edit" style="display: <?php echo isset($actionContact) ? "block" : "none"; ?>;">
        <form action="<?php echo "./contact.php?resource_id=$resourceGeneral->id" . $pageParams ?>" name="add_edit_contact" method="post">
            <table>
                <tr>
                    <th><b>ID</b></th>
                    <td>
                        <?php echo isset($actionContact) ? $actionContact->id : ""; ?>
                        <input type="hidden" name="id" value="<?php echo isset($actionContact) ? $actionContact->id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Организация ID</b></th>
                    <td>
                        <input type="text" name="organization_id" value="<?php echo isset($actionContact) ? $actionContact->organization_id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Имя</b></th>
                    <td>
                        <input type="text" name="name" value="<?php echo isset($actionContact) ? $actionContact->name : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Название</b></th>
                    <td>
                        <input type="text" name="title" value="<?php echo isset($actionContact) ? $actionContact->title : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Телефоны</b></th>
                    <td>
                        <input type="text" name="phone" value="<?php echo isset($actionContact) ? $actionContact->phone : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Сайт</b></th>
                    <td>
                        <input type="text" name="site" value="<?php echo isset($actionContact) ? $actionContact->site : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>E-mails</b></th>
                    <td>
                        <input type="text" name="email" value="<?php echo isset($actionContact) ? $actionContact->email : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>E-mails обновлены</b></th>
                    <td>
                        <input type="checkbox" name="emails_done" <?php echo (isset($actionContact) && $actionContact->emails_done) ? "checked" : ""; ?> />
                    </td>
                </tr>
                <tr>
                    <th><b>Закончен</b></th>
                    <td>
                        <input type="checkbox" name="done" <?php echo (isset($actionContact) && $actionContact->done) ? "checked" : ""; ?> />
                    </td>
                </tr>
                <tr>
                    <th><b>Дата создания</b></th>
                    <td>
                        <?php echo isset($actionContact) ? $actionContact->created_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Дата обновления</b></th>
                    <td>
                        <?php echo isset($actionContact) ? $actionContact->updated_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Дата последней отсылки</b></th>
                    <td>
                        <?php echo isset($actionContact) ? $actionContact->date_last_send : ""; ?>
                        <input type="hidden" name="date_last_send" value="<?php echo isset($actionContact) ? $actionContact->date_last_send : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <button type="submit">Сохранить</button>
                        </form>
                        &nbsp;&nbsp;&nbsp;<a href="<?php echo "./contact.php?resource_id=$resourceGeneral->id" . $pageParams ?>">Отмена</a>
                    </td>
                </tr>
            </table> 
        </div>

        <a id='href_to_update' href="<?php echo "./contact.php?resource_id=$resourceGeneral->id&action=update_next_organization_contacts" . $pageParams ?>">
            <h1>Обновить контакты организаций<b id='time_to_update'></b></h1>
        </a>
        <?php $numberSite = isset($_GET['num_site']) ? '&num_site=' . $_GET['num_site'] : '';
                $numberBlockContactPages = isset($_GET['num_block']) ? '&num_block=' . $_GET['num_block'] : ''; 
                $allParams = $numberSite . $numberBlockContactPages . $pageParams;
                ?>
        <a id='href_to_update_email' href="<?php echo "./contact.php?resource_id=$resourceGeneral->id&action=update_next_organization_emails" . $allParams ?>">
            <h1>Обновить email's организаций<b id='time_to_update_email'></b></h1>
        </a>
        <a href='<?php echo "./contact.php?resource_id=$resourceGeneral->id" . $pageParams ?>'>Отмена авто обновления</a>
            
        <h2>Статистика</h2>
        <table>
            <tr>
                <th><b>Всех организаций</b></th>
                <th><b>Законченых организаций</b></th>
                <th><b>Всех контактов</b></th>
                <th><b>Готовых контактов</b></th>
                <th><b>Незаконченых контактов организаций</b></th>
            </tr>
            <tr>
                <td><b><?php echo $countOrganizationsAll = MailerOrganization::getCountByResource($resourceGeneral->id) ?></b></td>
                <td style='color: green'><b><?php echo $countOrganizationsIsDone = MailerOrganization::getCountByResourceIsDone($resourceGeneral->id) ?></b></td>
                <td><b><?php echo $countContactsAll = MailerContact::getCountByResource($resourceGeneral->id) ?></b></td>
                <td style='color: green'><b><?php echo $countContactsIsDone = MailerContact::getCountByResourceIsDone($resourceGeneral->id) ?></b></td>
                <td style='color: red'><b><?php echo $countOrganizationsAll - $countContactsIsDone ?></b></td>
            </tr>
        </table>
        <table>
            <tr>
                <th><b>Всех контактов</b></th>
                <th><b>Контактов с обновлёнными e-mails</b></th>
                <th><b>Контактов с не обновлёнными e-mails</b></th>
            </tr>
            <tr>
                <td><b><?php echo $countContactsAll ?></b></td>
                <td style='color: green'><b><?php echo $countContactsIsDoneEmails = MailerContact::getCountByResourceIsDoneEmails($resourceGeneral->id) ?></b></td>
                <td style='color: red'><b><?php echo $countContactsAll - $countContactsIsDoneEmails ?></b></td>
            </tr>
        </table>
            
        <br/>
        <?php 
            $backLink = "-><a href='./resource.php'>$resourceGeneral->name</a>"
                    . "-><a href='./organization.php?resource_id=$resourceGeneral->id'>Организации</a>";
            
            $paginator = new Paginator();
            $paginator->countAllItems = $countContactsAll;
            $paginator->uri = "./contact.php?resource_id=$resourceGeneral->id";
            $paginator->activePage = isset($_GET['page']) ? $_GET['page'] : 1;
            $contentPagination = $paginator->render();
        ?>
        <h2><a href="./">Home</a><?php echo $backLink ?></h2>
        <h2>Список контактов организаций</h2>
        <br />
        <?php echo $contentPagination; ?>
        <br />
        <table>
            <tr>
                <th><b>#</b></th>
                <th><b>ID</b></th>
                <th><b>Организация ID</b></th>
                <th><b>Имя</b></th>
                <th><b>Название</b></th>
                <th><b>Телефоны</b></th>
                <th><b>Сайт</b></th>
                <th><b>E-mails</b></th>
                <th><b>E-mails обновлены</b></th>
                <th><b>Закончен</b></th>
                <th><b>Дата создания</b></th>
                <th><b>Дата обновления</b></th>
                <th><b>Дата последней отсылки</b></th>
                <th><b>Действия</b></th>
            </tr>
<?php             
        $contacts = MailerContact::getContactsByResource($resourceGeneral->id,
                ($paginator->activePage - 1) * $paginator->countItemsToPage, $paginator->countItemsToPage);
        
        $index = ($paginator->activePage - 1) * $paginator->countItemsToPage;
        foreach ($contacts as $key => $contact) {
            $mailerOrganization = new MailerOrganization($contact->organization_id);
            $outText = "<tr " . (($index % 2) ? "class='dark'" : "") . ">"
                . "<td>" . ($index++ + 1) . "</td>"
                . "<td>$contact->id</td>"
                . "<td>$contact->organization_id - $mailerOrganization->name</td>"
                . "<td>$contact->name</td>"
                . "<td>$contact->title</td>"
                . "<td>";
            
            $phones = explode(';', $contact->phone);
            foreach ($phones as $phone) {
                $outText .= "<span style='white-space: nowrap'>$phone</span><br />\n";
            }

            $outText .= "</td><td>";
            
            $sites = explode(';', $contact->site);
            foreach ($sites as $site) {
                $outText .= "<a style='white-space: nowrap' href='$site' target='_blank'>"
                        . "$site</a><br /><br />\n";
            }

            $outText .= "</td><td>";

            $emails = explode('|', $contact->email);
            foreach ($emails as $email) {
                if ($email == "") {
                    continue;
                }
                $outText .= "<span style='white-space: nowrap'>$email</span> <br />\n";
            }

            $outText .= "</td>"
                . "<td><b style='color:" . ($contact->emails_done ? "green'>Да" : "red'>Нет") . "</b></td>"
                . "<td><b style='color:" . ($contact->done ? "green'>Да" : "red'>Нет") . "</b></td>"
                . "<td>$contact->created_at</td>"
                . "<td>$contact->updated_at</td>"
                . "<td><b style='color:" . ($contact->date_last_send != "0000-00-00 00:00:00" ? "green'>" : "gray'>") . "$contact->date_last_send</b></td>"
                . "<td><table style='border: 0px;'>"
                    . "<tr><td><a href='./contact.php?resource_id=$resourceGeneral->id&id=$contact->id&action=delete$pageParams' onclick=\"return confirm('Вы уверены, что хотите удалить - \'$contact->title\'?')\">Удалить</a><br /><br /></td></tr>"
                    . "<tr><td><a href='./contact.php?resource_id=$resourceGeneral->id&id=$contact->id&action=edit$pageParams'>Редактировать</a><br /><br /></td></tr>"
                . "</table></td>"
            . "</tr>";
            echo $outText;
        }
?>
        </table>
        <br />
        <?php echo $contentPagination; ?>
    </body>    
</html>