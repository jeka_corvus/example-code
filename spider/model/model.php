<?php
class Model {
    protected $listRows;
            
    public function __construct() { }
    
    protected function mappingFromArray($arrayParams) {
        foreach ($arrayParams as $param => $value){
            $this->{$param} = $value;
        }
    }
    
    protected function getDateNow() {
        $date = new DateTime();
        return $date->format('Y-m-d H:i:s');
    }

    protected function mappingSQL($sql) {
        $pairsArgs = array();
        foreach ($this->listRows as $param) {
            if ($param == 'id' && $this->{$param} == 0)
                continue;
            $pairsArgs[] = "`$param`='" . DBController::getInstance()->escape($this->{$param}) . "'";
        }
        $sql .= join(', ', $pairsArgs);
        return $sql;
    }
    
}