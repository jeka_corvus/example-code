<?php
require_once('model.php');

class MailerResource extends Model {
    private static $tableName = "mailer_resource";
    
    public $id = 0;
    public $name;
    public $uri;
    public $script;
    public $uri_next_organization;
    public $done = 0;
    public $created_at;
    public $updated_at;
    
    public function __construct($id = 0) {
        parent::__construct();
        
        $this->listRows = array(
        'id', 
        'name', 
        'uri',
        'script',
        'uri_next_organization',
        'done',
        'created_at',
        'updated_at');
        
        if ($id != 0 && $id != "" && !empty($id)) {
            $sql = "SELECT * FROM `" . self::$tableName . "` WHERE `id`=" . $id;
            $result = DBController::getInstance()->query($sql);
            if (count($result->row) > 0) {
                $this->mappingFromArray($result->row);
            }
        }
    }
    
    public static function getResources($limitStart = 0, $limitCount = 0) {
        $sql = "SELECT * FROM `" . self::$tableName . "` ORDER BY `id` ASC";
        if ($limitCount != 0) {
            $sql .= " LIMIT $limitStart, $limitCount";
        }
        
        $result = DBController::getInstance()->query($sql);
        
        $listResources = array();
        foreach ($result->rows as $resources) {
            $mailerResource = new MailerResource();
            $mailerResource->mappingFromArray($resources);
            $listResources[] = $mailerResource;
        }
        
        return $listResources;
    }
    
    public function create() {
        $sql = "INSERT INTO `" . self::$tableName . "` SET ";

        $dateNow = $this->getDateNow();
        
        $this->created_at = $dateNow;
        $this->updated_at = $dateNow;
        
        $sql = $this->mappingSQL($sql);
        DBController::getInstance()->query($sql);

        $this->id = DBController::getInstance()->getLastId();
    }

    public function update() {
        $sql = "UPDATE `" . self::$tableName . "` SET ";

        $this->updated_at = $this->getDateNow();
        $sql = $this->mappingSQL($sql);

        $sql .= " WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public function delete() {
        $sql = "DELETE FROM `" . self::$tableName . "` WHERE `id`=" . $this->id;
        DBController::getInstance()->query($sql);
    }
    
    public static function getByEmailId($emailId) {
        $sql = "SELECT mr.* FROM `" . self::$tableName . "` as mr"
                . " INNER JOIN `mailer_email_resource` as mer ON mr.`id`=mer.`resource_id`"
                . " WHERE mer.`email_id`=$emailId ORDER BY mr.`id` ASC";
        $result = DBController::getInstance()->query($sql);
        
        $listResources = array();
        foreach ($result->rows as $resources) {
            $mailerResource = new MailerResource();
            $mailerResource->mappingFromArray($resources);
            $listResources[] = $mailerResource;
        }
        
        return $listResources;
    }
    
    public static function getCount() {
        $sql = "SELECT count(*) as count FROM `" . self::$tableName . "`";
        $result = DBController::getInstance()->query($sql);
        return $result->row['count'];
    }
}