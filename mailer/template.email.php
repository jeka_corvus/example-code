<?php
require_once('default.conf.php');
require_once('db.controller.php');
require_once('utility/paginator.php');

$pageParams = "";
if (isset($_GET['page'])) {
    $pageParams .= 'page=' . $_GET['page'];
}

if (isset($_POST['id'])) {
    $mailerEmail = new MailerEmail($_POST['id']);
    if (isset($_FILES['image_file'])) {
        foreach ($_FILES['image_file']['name'] as $key => $name) {
            $nameImage = "";
            if (isset($_FILES['image_file']['name'][$key])) {
                $nameImage = basename($_FILES['image_file']['name'][$key]);
                $targetFile = ConfigMailer::$rootDir . ConfigMailer::$dirImagesEmail . $nameImage;
                move_uploaded_file($_FILES['image_file']['tmp_name'][$key], $targetFile);
            } else {
                $nameImage = $image;
            }
            
            if ($nameImage != "") {
                $mailerEmail->image .= ($mailerEmail->image != "" ? ";" : "") . $nameImage;
            }
        }
        $mailerEmail->update();
        
        header("Location: ./template.email.php?id=$mailerEmail->id&action=edit" . ($pageParams == "" ? "" : "&") . $pageParams);
        
    } else if ($_POST['subject'] != "" && $_POST['body_email'] != "") {
        $mailerEmail->subject           = $_POST['subject'];
        $mailerEmail->from              = $_POST['from'];
        $mailerEmail->body_email        = $_POST['body_email'];
        $mailerEmail->image             = $_POST['image'];
        
        if ($_POST['id'] == "") {
            $mailerEmail->create();
        } else {
            $mailerEmail->update();
        }
        
        MailerEmailResource::deleteByEmailId($mailerEmail->id);
        if (isset($_POST['resource_list'])) {
            foreach ($_POST['resource_list'] as $resource_id) {
                $mailerEmailResource = new MailerEmailResource();
                $mailerEmailResource->email_id = $mailerEmail->id;
                $mailerEmailResource->resource_id = $resource_id;
                
                if (!$mailerEmailResource->isExist()) {
                    $mailerEmailResource->create();
                }
            }
        }
        
        header("Location: ./template.email.php" . ($pageParams == "" ? "" : "?") . $pageParams);
    }
}


if (isset($_GET['action'])) {
    if (isset($_GET['id'])) {
        $actionEmail = new MailerEmail($_GET['id']);
    }
    
    switch ($_GET['action']){
        case "edit":
            $listResources = MailerResource::getByEmailId($actionEmail->id);
            $listSelectedResources = array();
            foreach ($listResources as $resource) {
                $listSelectedResources[] = $resource->id;
            }
            break;
        
        case "delete":
            $actionEmail->delete();
            header("Location: ./template.email.php" . ($pageParams == "" ? "" : "?") . $pageParams);
            break;
        
        case "clear":
            MailerEmailContact::clear($actionEmail->id);
            header("Location: ./template.email.php"  . ($pageParams == "" ? "" : "?") . $pageParams);
            break;
        
        case "send_next_emails":
            
            $listContacts = MailerContact::getContactsIsNotSend($actionEmail->id, ConfigMailer::$countContactsSend);
//            $listContacts = MailerContact::getContactsIsNotSend($actionEmail->id, 1);
            
            if (count($listContacts) > 0) {
                require_once('utility/mailer.php');
                $mailer = new Mailer($actionEmail);
                $result = $mailer->sendEmails($listContacts);
                if ($result === true) {
                    //Максимальное количество одновременных подключений к почтовым сервисам
                    //— не более 10, интенсивность подключения к почтовым сервисам — не более 10
                    //соединений в минуту
                    header("Location: ./template.email.php?id=$actionEmail->id&auto_action=send_next_emails&time=65" .  ($pageParams == "" ? "" : "&") . $pageParams);
                } else {
                    header("Location: ./template.email.php?id=$actionEmail->id&auto_action=send_next_emails&time=600" .  ($pageParams == "" ? "" : "&") . $pageParams);
                }
            } else {
                $actionEmail->updateDateLastSend();
                header("Location: ./template.email.php" .  ($pageParams == "" ? "" : "?") . $pageParams);
            }
            break;

        default:
            header("Location: ./template.email.php"  . ($pageParams == "" ? "" : "?") . $pageParams);
            break;
    }
}

$listResource = MailerResource::getResources();
?>
<html dir="ltr" lang="ru">
    <head>
        <title>Mailer Template Email</title>
        <meta charset="UTF-8" />
        <script type="text/javascript" src="./js/utility.js"></script>
        <?php if (isset($_GET['auto_action']) && $_GET['auto_action'] == 'send_next_emails') { ?>
            <script type="text/javascript">startNextUpdateWithParams('href_to_update', 'time_to_update', <?php echo $_GET['time'] ?>);</script>
        <?php } ?>
        <style>
           table { 
            border: 4px double black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
           }
           th { 
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           td { 
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
           }
           .dark {
                background: #ccc; /* Цвет фона ячеек */
           }
           textarea {
               width: 1024px;
               height: 600px;
           }
           input[type=text] {
                width: 800px;
           }
           .active_page {
               font-size: 25px;
           }
        </style>          
    </head>
    <body>

    <h2><a href="#" onclick="showHideElement('block_add_edit'); return false;">Добавить/Редактировать шаблон письма</a></h2>
    <div id="block_add_edit" style="display: <?php echo isset($actionEmail) ? "block" : "none"; ?>;">
        <h2>Добавить\Редактировать шаблон E-mail</h2>
        <form action="<?php echo "./template.email.php"  . ($pageParams == "" ? "" : "?") . $pageParams ?>" name="add_edit_email" method="post">
            <table>
                <tr>
                    <th><b>ID</b></th>
                    <td>
                        <?php echo isset($actionEmail) ? $actionEmail->id : ""; ?>
                        <input type="hidden" name="id" value="<?php echo isset($actionEmail) ? $actionEmail->id : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Список получателей</b></th>
                    <td>
                        <select multiple size="10" name="resource_list[]">
                        <?php 
                            foreach ($listResource as $resource) {
                                $selected = "";
                                if (isset($listSelectedResources) 
                                        && in_array($resource->id, $listSelectedResources)) {
                                    $selected = " selected";
                                }
                                echo "<option$selected value='$resource->id'>$resource->id - $resource->name</option>";
                            }
                        ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><b>Тема</b></th>
                    <td>
                        <input type="text" name="subject" value="<?php echo isset($actionEmail) ? htmlspecialchars($actionEmail->subject) : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>От кого</b></th>
                    <td>
                        <input type="text" name="from" value="<?php echo isset($actionEmail) ? htmlspecialchars($actionEmail->from) : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th><b>Письмо</b></th>
                    <td>
                        <textarea name="body_email"><?php echo isset($actionEmail) ? $actionEmail->body_email : ""; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th><b>Изображения</b></th>
                    <td>
                        <input type='text'  name='image' value='<?php echo isset($actionEmail) ? $actionEmail->image : ""; ?>' />
            <?php   if (isset($actionEmail)) {
                        $listImages = explode(';', $actionEmail->image);
                        foreach ($listImages as $key => $image) {
                            echo "<br /><img src='." . ConfigMailer::$dirImagesEmail . $image . "' />";
                        }
                    } 
            ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Дата создания</b></th>
                    <td>
                        <?php echo isset($actionEmail) ? $actionEmail->created_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Дата обновления</b></th>
                    <td>
                        <?php echo isset($actionEmail) ? $actionEmail->updated_at : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th><b>Дата последней отсылки</b></th>
                    <td>
                        <?php echo isset($actionEmail) ? $actionEmail->date_last_send : ""; ?>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <button type="submit">Сохранить</button>
                        </form>
                        &nbsp;&nbsp;&nbsp;<a href="<?php echo "./template.email.php"  . ($pageParams == "" ? "" : "?") . $pageParams ?>">Отмена</a>
                    </td>
                </tr>
        <?php if (isset($actionEmail)) { ?>
                <tr><th colspan="2" style="text-align: center;">Форма для загрузки картинок(отдельная)</th></tr>
                <form action="<?php echo "./template.email.php?id=$actionEmail->id&action=edit" . ($pageParams == "" ? "" : "&") . $pageParams ?>" name="add_images" method="post" enctype="multipart/form-data">
                <tr>
                    <th><b>Загрузить изображения</b></th>
                    <td>
                        <input type="hidden" name="id" value="<?php echo isset($actionEmail) ? $actionEmail->id : ""; ?>" />
                        <input type='file' name='image_file[]' multiple />
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <button type="submit">Сохранить</button>
                        </form>
                        &nbsp;&nbsp;&nbsp;<a href="<?php echo "./template.email.php"  . ($pageParams == "" ? "" : "?") . $pageParams ?>">Отмена</a>
                    </td>
                </tr>
        <?php } ?>
            </table>
    </div>
    <?php if (isset($_GET['auto_action']) && $_GET['auto_action'] == 'send_next_emails') { 
        $autoActionEmail = new MailerEmail($_GET['id']);
    ?>
        <a id='href_to_update' href="<?php echo "./template.email.php?id=" . $autoActionEmail->id . "&action=send_next_emails" . ($pageParams == "" ? "" : "&") . $pageParams ?>">
            <h1>Отправить письма<b id='time_to_update'></b></h1>
        </a>
        <a href='<?php echo "./template.email.php" . ($pageParams == "" ? "" : "?") . $pageParams ?>'>Отмена авто отправки</a>
            
        <h2>Статистика</h2>
        <table>
            <tr>
                <th><b>Письмо</b></th>
                <th><b>Всех контактов</b></th>
                <th><b>Отправленных контактов</b></th>
                <th><b>Неотправленных контактов</b></th>
                <th><b>Всех e-mails</b></th>
                <th><b>Отправленных e-mails</b></th>
                <th><b>Неотправленных e-mails</b></th>
            </tr>
            <tr>
                <td><b><?php echo $autoActionEmail->id . " - " . $autoActionEmail->subject ?></b></td>
                <td><b><?php echo $countContactsToSend = MailerContact::getCountByEmail($autoActionEmail->id) ?></b></td>
                <td style='color: green'><b><?php echo $countContactsIsSend = MailerEmailContact::getCountByEmail($autoActionEmail->id) ?></b></td>
                <td style='color: red'><b><?php echo $countContactsToSend - $countContactsIsSend ?></b></td>
                <td><b><?php echo $countContactEmailsToSend = MailerContact::getCountEmailsByEmailId($autoActionEmail->id) ?></b></td>
                <td style='color: green'><b><?php echo $countContactEmailsIsSend = MailerEmailContact::getCountEmailsByEmailId($autoActionEmail->id) ?></b></td>
                <td style='color: red'><b><?php echo $countContactEmailsToSend - $countContactEmailsIsSend ?></b></td>
            </tr>
        </table>
    <?php } ?>
            
        <br/>
        <?php 
            $paginator = new Paginator();
            $paginator->countAllItems = MailerEmail::getCount();
            $paginator->uri = "./template.email.php?";
            $paginator->activePage = isset($_GET['page']) ? $_GET['page'] : 1;
            $contentPagination = $paginator->render();
        ?>
        <h2><a href="./">Home</a></h2>
        <h2>Список писем</h2>
        <br />
        <?php echo $contentPagination; ?>
        <br />
        <table>
            <tr>
                <th><b>#</b></th>
                <th><b>ID</b></th>
                <th><b>Тема</b></th>
                <th><b>От кого</b></th>
                <th><b>Список получателей</b></th>
                <th><b>Дата создания</b></th>
                <th><b>Дата обновления</b></th>
                <th><b>Дата последней отсылки</b></th>
                <th><b>Действия</b></th>
            </tr>
<?php             
        $emails = MailerEmail::getTemplateEmails(($paginator->activePage - 1) * $paginator->countItemsToPage, 
                                        $paginator->countItemsToPage);
        
        $index = ($paginator->activePage - 1) * $paginator->countItemsToPage;
        foreach ($emails as $key => $email) {
            $outText = "<tr " . (($index % 2) ? "class='dark'" : "") . ">\n"
                . "<td>" . ($index++ + 1) . "</td>\n"
                . "<td>$email->id</td>\n"
                . "<td>" . htmlspecialchars($email->subject) . "</td>\n"
                . "<td>" . htmlspecialchars($email->from) . "</td>\n"
                . "<td><ul>\n";
            
            $listResources = MailerResource::getByEmailId($email->id);
            foreach ($listResources as $resource) {
                $outText .= "<li>$resource->id - $resource->name</li>\n";
            }
            
            $outText .= "</ul></td>\n"
                . "<td>$email->created_at</td>\n"
                . "<td>$email->updated_at</td>\n"
                . "<td>$email->date_last_send</td>\n"
                . "<td><table style='border: 0px;'>\n"
                    . "<tr><td><a href='./template.email.php?id=$email->id&action=delete" . ($pageParams == "" ? "" : "&") . $pageParams . "' onclick=\"return confirm('Вы уверены, что хотите удалить - \'$email->subject\'?')\">Удалить</a><br /><br /></td></tr>\n"
                    . "<tr><td><a href='./template.email.php?id=$email->id&action=edit" . ($pageParams == "" ? "" : "&") . $pageParams . "'>Редактировать</a><br /><br /></td></tr>"
                    . "<tr><td><a href='./template.email.php?id=$email->id&action=send_next_emails" . ($pageParams == "" ? "" : "&") . $pageParams . "' onclick=\"return confirm('Вы уверены, что хотите начать расслыку - \'$email->subject\'?')\">Начать рассылку</a><br /><br /></td></tr>\n"
                    . "<tr><td><a href='./template.email.php?id=$email->id&action=clear" . ($pageParams == "" ? "" : "&") . $pageParams . "' onclick=\"return confirm('Вы уверены, что хотите обнулить расслыку - \'$email->subject\'?')\">Обнулить рассылку</a><br /><br /></td></tr>\n"
                . "</table></td>\n"
            . "</tr>\n";
            echo $outText;
        }
?>
        </table>
        <br />
        <?php echo $contentPagination; ?>
    </body>    
</html>